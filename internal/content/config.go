package content

import (
	"context"
	"fmt"

	"github.com/gocql/gocql"
	"gitlab.com/jgero/host-my-club/internal/constants"
	"golang.org/x/crypto/bcrypt"
)

type ClubConfig struct {
	Id          string
	Name        string
	Shorthand   string
	CC          string
	Description string
	Palette     ColorPalette
}

type ColorPalette struct {
	Primary     string `cql:"primarycolor"`
	Accent      string `cql:"accent"`
	Sheet       string `cql:"sheet"`
	Background  string `cql:"background"`
	Font        string `cql:"font"`
	FontPrimary string `cql:"fontprimary"`
	FontAccent  string `cql:"fontaccent"`
}

type ConfigDto interface {
	LoadConfigToContext(ctx context.Context) (context.Context, error)
	CreateNewClub(ctx context.Context, cfg ClubConfig, email string, password string, username string) (string, error)
	UpdateClubConfig(context.Context, ClubConfig) error
	DeleteClub(context.Context, string) error
}

type ConfigMissingError struct {
	ClubId string
}

func (e *ConfigMissingError) Error() string {
	return fmt.Sprintf("config for club with id '%s' is missing", e.ClubId)
}

type ClubIdTakenError struct {
	ClubId string
}

func (e *ClubIdTakenError) Error() string {
	return fmt.Sprintf("a club with id '%s' already exists", e.ClubId)
}

type ConfigCassandraDto struct {
	Cluster *gocql.ClusterConfig
}

func (dto ConfigCassandraDto) LoadConfigToContext(ctx context.Context) (context.Context, error) {
	session, err := dto.Cluster.CreateSession()
	if err != nil {
		return nil, err
	}
	defer session.Close()

	clubId, ok := ctx.Value(constants.ClubIdKey).(string)
	if !ok {
		return nil, fmt.Errorf("club name missing in context")
	}

	cfg := ClubConfig{Id: clubId, Palette: ColorPalette{}}

	if err := session.Query(`SELECT name, shorthand, cc, description, palette FROM club WHERE clubid = ?`, clubId).WithContext(ctx).Scan(&(cfg.Name), &(cfg.Shorthand), &(cfg.CC), &(cfg.Description), &(cfg.Palette)); err != nil {
		return nil, &ConfigMissingError{ClubId: clubId}
	}
	ctx = context.WithValue(ctx, constants.ClubConfigKey, cfg)
	return ctx, nil
}

func (dto ConfigCassandraDto) CreateNewClub(ctx context.Context, cfg ClubConfig, email string, password string, username string) (string, error) {
	session, err := dto.Cluster.CreateSession()
	if err != nil {
		return "", err
	}
	defer session.Close()

	userid := gocql.TimeUUID()
	passwordHash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}

	// check first if this clubid is still available, no error means already taken
	var _temp string
	if err := session.Query(`SELECT clubid FROM club WHERE clubid = ?`, cfg.Id).Scan(&_temp); err == nil {
		return "", &ClubIdTakenError{ClubId: cfg.Id}
	}

	// insert via logged batch to ensure data consistency
	batch := session.NewBatch(gocql.LoggedBatch).WithContext(ctx)
	batch.Query(`INSERT INTO club (clubid, name, shorthand, cc, description, palette) VALUES (?, ?, ?, ?, ?, ?)`, cfg.Id, cfg.Name, cfg.Shorthand, cfg.CC, cfg.Description, cfg.Palette)
	batch.Query(`INSERT INTO user_by_email (clubid, email, passwordhash, userid) VALUES (?, ?, ?, ?)`, cfg.Id, email, string(passwordHash), userid)
	batch.Query(`INSERT INTO user_by_id (clubid, userid, name, email, groups) VALUES (?, ?, ?, ?, ?)`, cfg.Id, userid, username, email, constants.AdminGroup)
	if err := session.ExecuteBatch(batch); err != nil {
		return "", err
	}

	return cfg.Id, nil
}

func (dto ConfigCassandraDto) UpdateClubConfig(ctx context.Context, cfg ClubConfig) error {
	session, err := dto.Cluster.CreateSession()
	if err != nil {
		return err
	}
	defer session.Close()

	if err := session.Query(`UPDATE club SET name = ?, shorthand = ?, description = ?, palette = ? WHERE clubid = ?`, cfg.Name, cfg.Shorthand, cfg.Description, cfg.Palette, cfg.Id).WithContext(ctx).Exec(); err != nil {
		return err
	}
	return nil
}

func (dto ConfigCassandraDto) DeleteClub(ctx context.Context, clubid string) error {
	session, err := dto.Cluster.CreateSession()
	if err != nil {
		return err
	}
	defer session.Close()

	// delete entries in all tables for that club
	batch := session.NewBatch(gocql.LoggedBatch).WithContext(ctx)
	batch.Query(`DELETE FROM club WHERE clubid = ?`, clubid)
	batch.Query(`DELETE FROM user_by_email WHERE clubid = ?`, clubid)
	batch.Query(`DELETE FROM user_by_id WHERE clubid = ?`, clubid)
	if err := session.ExecuteBatch(batch); err != nil {
		return err
	}
	return nil
}
