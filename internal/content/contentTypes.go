package content

type MetaTags struct {
	Title        string
	Description  string
	CanonicalUrl string
	ImageUrl     string
}
