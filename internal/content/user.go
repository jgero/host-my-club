package content

import (
	"context"
	"fmt"
	"sort"
	"strings"

	"github.com/gocql/gocql"
	"gitlab.com/jgero/host-my-club/internal/constants"
	"golang.org/x/crypto/bcrypt"
)

type UserDto interface {
	CreateNewUser(ctx context.Context, email string, password string, username string) (string, error)
	GetUserByEmail(ctx context.Context, email string) (*UserByEmail, error)
	VerifyPassword(ctx context.Context, email string, password string) (bool, error)
	GetUserById(ctx context.Context, userid string) (*UserById, error)
	GetAllUsers(ctx context.Context) ([]UserById, error)
	UpdateUsername(ctx context.Context, updateTargetId string, newUsername string) error
	UpdateUserGroups(ctx context.Context, updateTargetId string, newGroups []string) error
	DeleteUser(ctx context.Context, userid string) error
}

type UserByEmail struct {
	Email  string
	Userid string
}

type UserById struct {
	Username string
	Userid   string
	Email    string
	Groups   []string
	IsAdmin  bool
	IsEditor bool
}

// VERY IMPORTANT NOTE !!!!!!!!!!!!!!!!
// "groups" in the cassandra user_by_id table is a single text field! It consits of the groups in
// alphanumerical order, separated by a single "," char.

type UserCassandraDto struct {
	Cluster *gocql.ClusterConfig
}

type EmailTakenError struct {
	Email string
}

func (e *EmailTakenError) Error() string {
	return fmt.Sprintf("email '%s' is already taken", e.Email)
}

func (dto UserCassandraDto) CreateNewUser(ctx context.Context, email string, password string, username string) (string, error) {
	session, err := dto.Cluster.CreateSession()
	if err != nil {
		return "", err
	}
	defer session.Close()

	userid := gocql.TimeUUID()
	passwordHash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}

	clubid, ok := ctx.Value(constants.ClubIdKey).(string)
	if !ok || clubid == "" {
		return "", fmt.Errorf("clubid missing in context")
	}

	// check first if this email is still available, no error means already taken
	var _temp string
	if err := session.Query(`SELECT userid FROM user_by_email WHERE clubid = ? AND email = ?`, clubid, email).Scan(&_temp); err == nil {
		return "", &EmailTakenError{Email: email}
	}

	// insert via logged batch to ensure data consistency. if because of eventual consistency some
	// entry got written into the user_by_email table since the check in the lines before, someone
	// really ran out of luck that day, the data will just be overwritten...
	batch := session.NewBatch(gocql.LoggedBatch).WithContext(ctx)
	batch.Query(`INSERT INTO user_by_email (clubid, email, passwordhash, userid) VALUES (?, ?, ?, ?)`, clubid, email, string(passwordHash), userid)
	batch.Query(`INSERT INTO user_by_id (clubid, userid, name, email, groups) VALUES (?, ?, ?, ?, ?)`, clubid, userid, username, email, constants.NoGroup)
	if err := session.ExecuteBatch(batch); err != nil {
		return "", err
	}

	return userid.String(), nil
}

func (dto UserCassandraDto) VerifyPassword(ctx context.Context, email string, password string) (bool, error) {
	session, err := dto.Cluster.CreateSession()
	if err != nil {
		return false, err
	}
	defer session.Close()

	clubid, ok := ctx.Value(constants.ClubIdKey).(string)
	if !ok || clubid == "" {
		return false, fmt.Errorf("clubid missing in context")
	}

	var passwordHash []byte
	if err := session.Query(`SELECT passwordhash from user_by_email WHERE clubid = ? AND email = ?`, clubid, email).WithContext(ctx).Scan(&passwordHash); err != nil {
		return false, err
	}

	// the bcrypt compare function returns an error if hash and pw do not match
	if err := bcrypt.CompareHashAndPassword(passwordHash, []byte(password)); err != nil {
		return false, nil
	} else {
		return true, nil
	}
}

type UserMissingError struct {
	Key string
}

func (e *UserMissingError) Error() string {
	return fmt.Sprintf("user with email or id '%s' is missing", e.Key)
}

func (dto UserCassandraDto) GetUserByEmail(ctx context.Context, email string) (*UserByEmail, error) {
	session, err := dto.Cluster.CreateSession()
	if err != nil {
		return nil, err
	}
	defer session.Close()

	clubid, ok := ctx.Value(constants.ClubIdKey).(string)
	if !ok || clubid == "" {
		return nil, fmt.Errorf("clubid missing in context")
	}

	var userid string
	if err := session.Query(`SELECT userid FROM user_by_email WHERE clubid = ? AND email = ?`, clubid, email).WithContext(ctx).Scan(&userid); err != nil {
		if err.Error() == gocql.ErrNotFound.Error() {
			return nil, &UserMissingError{Key: email}
		} else {
			return nil, err
		}
	}
	return &UserByEmail{Email: email, Userid: userid}, nil
}

func (dto UserCassandraDto) GetUserById(ctx context.Context, userid string) (*UserById, error) {
	session, err := dto.Cluster.CreateSession()
	if err != nil {
		return nil, err
	}
	defer session.Close()

	clubid, ok := ctx.Value(constants.ClubIdKey).(string)
	if !ok || clubid == "" {
		return nil, fmt.Errorf("clubid missing in context")
	}

	var name, email, groups string
	if err := session.Query(`SELECT name, email, groups FROM user_by_id WHERE clubid = ? AND userid = ?`, clubid, userid).Scan(&name, &email, &groups); err != nil {
		if err.Error() == gocql.ErrNotFound.Error() {
			return nil, &UserMissingError{Key: userid}
		} else {
			return nil, err
		}
	}
	groupArr := groupStringToArr(groups)
	return &UserById{
		Username: name,
		Userid:   userid,
		Email:    email,
		Groups:   groupArr,
		IsAdmin:  checkForGroup(groupArr, constants.AdminGroup),
		IsEditor: checkForGroup(groupArr, constants.EditorGroup),
	}, nil
}

func (dto UserCassandraDto) GetAllUsers(ctx context.Context) ([]UserById, error) {
	session, err := dto.Cluster.CreateSession()
	if err != nil {
		return nil, err
	}
	defer session.Close()

	returnVal := []UserById{}
	clubid, ok := ctx.Value(constants.ClubIdKey).(string)
	if !ok || clubid == "" {
		return nil, fmt.Errorf("clubid missing in context")
	}

	scanner := session.Query(`SELECT userid, name, email, groups FROM user_by_id WHERE clubid = ?`, clubid).WithContext(ctx).Iter().Scanner()
	for scanner.Next() {
		var userid, name, email, groups string
		err := scanner.Scan(&userid, &name, &email, &groups)
		if err != nil {
			return returnVal, err
		}

		groupArr := groupStringToArr(groups)

		returnVal = append(returnVal, UserById{
			Username: name,
			Userid:   userid,
			Email:    email,
			Groups:   groupArr,
			IsAdmin:  checkForGroup(groupArr, constants.AdminGroup),
			IsEditor: checkForGroup(groupArr, constants.EditorGroup),
		})
	}
	return returnVal, nil
}

func (dto UserCassandraDto) UpdateUsername(ctx context.Context, updateTargetId string, newUsername string) error {
	session, err := dto.Cluster.CreateSession()
	if err != nil {
		return err
	}
	defer session.Close()

	clubid, ok := ctx.Value(constants.ClubIdKey).(string)
	if !ok || clubid == "" {
		return fmt.Errorf("clubid missing in context")
	}

	if err := session.Query(`UPDATE user_by_id SET name = ? WHERE clubid = ? AND userid = ?`, newUsername, clubid, updateTargetId).WithContext(ctx).Exec(); err != nil {
		return err
	}
	return nil
}

func (dto UserCassandraDto) UpdateUserGroups(ctx context.Context, updateTargetId string, newGroups []string) error {
	session, err := dto.Cluster.CreateSession()
	if err != nil {
		return err
	}
	defer session.Close()

	clubid, ok := ctx.Value(constants.ClubIdKey).(string)
	if !ok || clubid == "" {
		return fmt.Errorf("clubid missing in context")
	}

	if err := session.Query(`UPDATE user_by_id SET groups = ? WHERE clubid = ? AND userid = ?`, groupArrToString(newGroups), clubid, updateTargetId).WithContext(ctx).Exec(); err != nil {
		return err
	}
	return nil
}

func (dto UserCassandraDto) DeleteUser(ctx context.Context, userid string) error {
	session, err := dto.Cluster.CreateSession()
	if err != nil {
		return err
	}
	defer session.Close()

	clubid, ok := ctx.Value(constants.ClubIdKey).(string)
	if !ok || clubid == "" {
		return fmt.Errorf("clubid missing in context")
	}

	userbyid, err := dto.GetUserById(ctx, userid)
	if err != nil {
		return fmt.Errorf("could not get email for user: %s", err)
	}

	// delte alle user documents via batch for data integrity
	batch := session.NewBatch(gocql.LoggedBatch).WithContext(ctx)
	batch.Query(`DELETE FROM user_by_email WHERE clubid = ? AND email = ?`, clubid, userbyid.Email)
	batch.Query(`DELETE FROM user_by_id WHERE clubid = ? AND userid = ?`, clubid, userid)
	if err := session.ExecuteBatch(batch); err != nil {
		return err
	}
	return nil
}

func checkForGroup(groups []string, groupToBeChecked string) bool {
	for _, grp := range groups {
		if grp == groupToBeChecked {
			return true
		}
	}
	return false
}

func groupStringToArr(groups string) []string {
	if groups == constants.NoGroup {
		return []string{}
	}
	val := strings.Split(groups, ",")
	sort.Strings(val)
	return val
}

func groupArrToString(groups []string) string {
	if len(groups) == 0 {
		return constants.NoGroup
	}
	sort.Strings(groups)
	return strings.Join(groups, ",")
}
