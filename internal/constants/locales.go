package constants

import "fmt"

type Locales struct {
	// titles
	LoginTitle    string
	RegisterTitle string
	ProfileTitle  string
	ErrorTitle    string
	// descriptions/subheaders
	TocSubheader        string
	FootnotesSubheader  string
	MyDataSection       string
	AdminPanelSection   string
	GroupManagerSection string
	// labels
	EmailLabel     string
	GroupsLabel    string
	AdminLabel     string
	EditorLabel    string
	UsernameLabel  string
	PasswordLabel  string
	NoAccountLabel string
	WelcomeLabel   string
	// brand
	BrandName      string
	WelcomeMessage string
	// messages
	PasswordWrongMessage string
	EmailTakenMessage    string
	Error500Message      string
	Error404Message      string
	Error401Message      string
	// call to action
	ShowAction string
	ReadAction string
	// buttons
	LoginButton    string
	LogoutButton   string
	RegisterButton string
	SaveButton     string
	// navbars
	// only internal links allowed
	TopNavbar []NavbarLink
	// only external links allowed
	Footer []NavbarLink
}

type NavbarLink struct {
	Title string
	Link  string
}

var localesEN = Locales{
	// titles
	LoginTitle:    "Login",
	RegisterTitle: "Register",
	ProfileTitle:  "Profile",
	ErrorTitle:    "Error",
	// descriptions/subheaders
	TocSubheader:        "Table of Contents",
	FootnotesSubheader:  "Footnotes",
	MyDataSection:       "My Data",
	AdminPanelSection:   "Admin Panel",
	GroupManagerSection: "Group Manager",
	// labels
	EmailLabel:     "E-Mail",
	GroupsLabel:    "Grups",
	AdminLabel:     "Admin",
	EditorLabel:    "Editor",
	UsernameLabel:  "Username",
	PasswordLabel:  "Password",
	NoAccountLabel: "You don't have an account? Sign up here!",
	WelcomeLabel:   "Welcome",
	// brand
	BrandName:      "Host my Club",
	WelcomeMessage: "Welcome at 'Host my Club'",
	// messages
	PasswordWrongMessage: "Password is wrong",
	EmailTakenMessage:    "E-Mail is already in use",
	Error500Message:      "Internal server error",
	Error404Message:      "This page does not exist",
	Error401Message:      "You are not allowed to see this page. Log out and back in to refresh your authorization.",
	// call to action
	ShowAction: "more",
	ReadAction: "read",
	// buttons
	LoginButton:    "Login",
	LogoutButton:   "Logout",
	RegisterButton: "Register",
	SaveButton:     "Save",

	TopNavbar: []NavbarLink{
		{Title: "Timeline", Link: "/timeline"},
		{Title: "Posts", Link: "/posts"},
	},
	Footer: []NavbarLink{
		{Title: "Githbu", Link: "https://example.com"},
		{Title: "Gitlab", Link: "https://example.com"},
	},
}

var localesDE = Locales{
	// titles
	LoginTitle:    "Login",
	RegisterTitle: "Registrieren",
	ProfileTitle:  "Profil",
	ErrorTitle:    "Fehler",
	// descriptions/subheaders
	TocSubheader:        "Inhaltsverzeichnis",
	FootnotesSubheader:  "Fußnoten",
	MyDataSection:       "Meine Daten",
	AdminPanelSection:   "Admin Panel",
	GroupManagerSection: "Gruppen Manager",
	// labels
	EmailLabel:     "E-Mail",
	GroupsLabel:    "Gruppen",
	AdminLabel:     "Admin",
	EditorLabel:    "Editor",
	UsernameLabel:  "Benutzername",
	PasswordLabel:  "Passwort",
	NoAccountLabel: "Du hast keinen Account? Hier registrieren!",
	WelcomeLabel:   "Willkommen",
	// brand
	BrandName:      "Host my Club",
	WelcomeMessage: "Willkommen bei 'Host my Club'",
	// messages
	PasswordWrongMessage: "Passwort ist falsch",
	EmailTakenMessage:    "E-Mail ist bereits vergeben",
	Error500Message:      "Interner Server Fehler",
	Error404Message:      "Seite existiert nicht",
	Error401Message:      "Du hast keine Berechtigung für diesen Inhalt. Logge dich auch und wieder ein um deine Berechtigungen neu zu laden.",
	// call to action
	ShowAction: "mehr",
	ReadAction: "lesen",
	// buttons
	LoginButton:    "Login",
	LogoutButton:   "Logout",
	RegisterButton: "Registrieren",
	SaveButton:     "Speichern",

	TopNavbar: []NavbarLink{
		{Title: "Zeitstrahl", Link: "timeline"},
		{Title: "Posts", Link: "posts"},
	},
	Footer: []NavbarLink{
		{Title: "Githbu", Link: "https://example.com"},
		{Title: "Gitlab", Link: "https://example.com"},
	},
}

func GetLocalesForCC(cc string) (Locales, error) {
	switch cc {
	case "en":
		return localesEN, nil
	case "de":
		return localesDE, nil
	default:
		return Locales{}, fmt.Errorf("there are no locales for this country code")
	}
}
