package constants_test

import (
	"testing"

	"gitlab.com/jgero/host-my-club/internal/constants"
)

type clubRouteTestcases struct {
	Route          string
	ExpectedResult bool
}

func TestClubRouteRegexp(t *testing.T) {
	tc := []clubRouteTestcases{
		{"/testclub/login", true},
		{"/testclub/register", true},
		{"/testclub/asdf", false},
		{"/testclub/user", false},
		{"/testclub/user/4993bc78-3fd0-11ec-abbf-9ed65005a454", true},
		{"/testclub/user/4993bc78-3fd0-11ec-abbf-9ed65005a454/rename", true},
		{"/testclub/user/4993bc78-3fd0-11ec-abbf-9ed65005a454/delete", true},
		{"/testclub/user/4993bc78-3fd0-11ec-abbf-9ed65005a454/asdf", false},
		{"/testclub", true},
	}

	for _, testcase := range tc {
		if testcase.ExpectedResult != constants.ClubRouteRegex().MatchString(testcase.Route) {
			t.Errorf("unexpected result for route %s: %v", testcase.Route, !testcase.ExpectedResult)
		}

	}
}

type clubIdTestcases struct {
	Id             string
	ExpectedResult bool
}

func TestClubIdChecking(t *testing.T) {
	tc := []clubIdTestcases{
		{"asdf", true},
		{"testclub", true},
		{"fcr", true},
		{"s04", true},
		{"system", false},
		{"login", false},
		{"a", false},
		{"asdfasdfasdfasdfasdfasdfasdf", false},
	}

	for _, testcase := range tc {
		if constants.CheckIsClubNameValid(testcase.Id) != testcase.ExpectedResult {
			t.Errorf("unexpected result for club %s: %v", testcase.Id, !testcase.ExpectedResult)
		}
	}
}
