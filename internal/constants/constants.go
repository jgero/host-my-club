package constants

import (
	"fmt"
	"regexp"
	"strings"
)

const (
	// prefixes for the different club routes
	PostRoute     = "post"
	UserRoute     = "user"
	UpdateRoute   = "update"
	AuthRoute     = "auth"
	LoginRoute    = "login"
	LogoutRoute   = "logout"
	RegisterRoute = "register"
	// suffixes for club routes
	RenameRoute = "rename"
	DeleteRoute = "delete"
	GroupsRoute = "groups"
	// prefixes for the system routes
	ErrorRoute      = "error"
	WelcomeRoute    = "easily"
	CreateClubRoute = "create"
	StaticRoute     = "static"
	// system constants
	SystemClubName   = "system"
	SystemLocalesKey = "hostmyclub-locales"
	// context keys
	RequestIDKey   = "request-id"
	RequestPathKey = "request-path"
	ClubIdKey      = "club-id"
	ClubConfigKey  = "club-config"
	UseridKey      = "auth-userid"
	IsAdminKey     = "auth-is-admin"
	IsEditorKey    = "auth-is-editor"
	// env variables
	HmacSecretEnvVar = "HMAC_AUTH_SECRET" // #nosec
	// utility groups
	AdminGroup  = "admin"
	EditorGroup = "editor"
	NoGroup     = "NONE"
)

func UuidRegexpString() string {
	return "[0-9a-fA-F]{8}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{12}"
}

func ClubNameRegexpString() string {
	return "[a-z0-9]{3,20}"
}

func ClubRouteRegex() *regexp.Regexp {
	standalonePrefixes := []string{
		AuthRoute,
		LoginRoute,
		LogoutRoute,
		RegisterRoute,
		DeleteRoute,
		UpdateRoute,
	}
	suffixes := []string{
		RenameRoute,
		GroupsRoute,
		DeleteRoute,
	}
	return regexp.MustCompile(fmt.Sprintf("^/%s(/(%s)|/(%s/%s(/(%s))?))?$",
		ClubNameRegexpString(),
		strings.Join(standalonePrefixes, "|"),
		UserRoute,
		UuidRegexpString(),
		strings.Join(suffixes, "|"),
	))
}

// check for any system club route
// the static prefix is excluded here, it has to be handled separatly
func SystemRouteRegex() *regexp.Regexp {
	return regexp.MustCompile(
		fmt.Sprintf("^/%s$",
			strings.Join([]string{
				ErrorRoute,
				WelcomeRoute,
				CreateClubRoute,
			}, "|"),
		),
	)
}

func CheckIsClubNameValid(name string) bool {
	// has to match regexp
	if !regexp.MustCompile(fmt.Sprintf("^%s$", ClubNameRegexpString())).MatchString(name) {
		return false
	}
	keywordList := []string{
		PostRoute,
		UserRoute,
		AuthRoute,
		LoginRoute,
		LogoutRoute,
		RegisterRoute,
		RenameRoute,
		DeleteRoute,
		UpdateRoute,
		GroupsRoute,
		ErrorRoute,
		WelcomeRoute,
		CreateClubRoute,
		SystemClubName,
	}
	// is not allowed to contain any route keywords or system club name
	for _, v := range keywordList {
		if strings.Contains(name, v) {
			return false
		}
	}
	return true
}

// -------------- extract parameters from routes, expects routes to be valid

func GetClubNameFromClubRoute(route string) string {
	return strings.Split(route, "/")[1]
}
