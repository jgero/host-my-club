package webserver

import (
	"context"
	"net/http"
)

// middleware that validates the form and loads all params into the context
func (*Webserver) formToContext() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if err := r.ParseForm(); err != nil {
				w.WriteHeader(http.StatusBadRequest)
				return
			}
			ctx := r.Context()
			for key, val := range r.Form {
				ctx = context.WithValue(ctx, key, val[0])
			}
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}

}
