package webserver

import (
	"fmt"
	"os"

	"github.com/golang-jwt/jwt/v4"
	"gitlab.com/jgero/host-my-club/internal/constants"
)

type AuthJwtConverter struct {
	Secret []byte
}

type JwtClaims struct {
	Userid   string `json:"userid"`
	IsAdmin  bool   `json:"is_admin"`
	IsEditor bool   `json:"is_editor"`
	jwt.RegisteredClaims
}

// extracts the json web token claims from a token string
func (c AuthJwtConverter) GetClaimsFromToken(tokenString string) (*JwtClaims, error) {
	token, err := jwt.ParseWithClaims(tokenString, &JwtClaims{}, func(token *jwt.Token) (interface{}, error) {
		// check if the token was signed with the right alg
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return c.Secret, nil
	})
	if err != nil {
		return nil, fmt.Errorf("could not parse token with claims: %s", err)
	}
	if !token.Valid {
		return nil, fmt.Errorf("token is not valid")
	}
	if claims, ok := token.Claims.(*JwtClaims); !ok {
		return nil, fmt.Errorf("claims are not of type JwtClaims")
	} else {
		return claims, nil
	}
}

// builds a json web token string from given claims
func (c AuthJwtConverter) BuildTokenFromClaims(claims JwtClaims) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	secret, exists := os.LookupEnv(constants.HmacSecretEnvVar)
	if !exists {
		return "", fmt.Errorf("environment variable '%s' for hmac secret is not set", constants.HmacSecretEnvVar)
	}
	tokenString, err := token.SignedString([]byte(secret))
	if err != nil {
		return "", fmt.Errorf("signing token failed: %s", err)
	}
	return tokenString, nil
}
