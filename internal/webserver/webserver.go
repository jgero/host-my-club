package webserver

import (
	"context"
	"embed"
	"fmt"
	"io/fs"
	"log"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"time"

	"gitlab.com/jgero/host-my-club/internal/constants"
	"gitlab.com/jgero/host-my-club/internal/content"
)

type Webserver struct {
	Port string

	UserDto   content.UserDto
	ConfigDto content.ConfigDto

	logger      *log.Logger
	isHealthy   bool
	healthMutex *sync.Mutex
}

//go:generate rm -rf static_copy
//go:generate cp -ru ../../web/static ./static_copy
//go:embed static_copy/*
var staticFiles embed.FS

func (serv *Webserver) Init() {
	// init health
	serv.isHealthy = false
	serv.healthMutex = &sync.Mutex{}

	serv.logger = log.New(os.Stdout, "http server: ", log.LstdFlags)
}

func (serv *Webserver) Start() {
	serv.logger.Println("server is starting...")

	router := http.NewServeMux()

	// add health check endpoint
	router.Handle("/health", serv.healthz())

	// trim the "static_copy"
	if trimmedStaticFs, err := fs.Sub(staticFiles, "static_copy"); err != nil {
		serv.logger.Printf("could not get subdir of static files: %s\n", err.Error())
	} else {
		// serve the trimmed static fs
		router.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.FS(trimmedStaticFs))))
	}

	secret, exists := os.LookupEnv(constants.HmacSecretEnvVar)
	if !exists {
		panic(fmt.Sprintf("environment variable '%s' for hmac secret is not set\n", constants.HmacSecretEnvVar))
	}
	router.Handle("/", serv.Authentication(AuthJwtConverter{[]byte(secret)}, serv.UserDto)(serv.DynamicRouteHandler()))

	// apply middlewares, logger and timeouts
	server := &http.Server{
		Addr: serv.Port,
		// chain the middlewares and add router as last in the chain
		Handler:
		// first do logging
		serv.logging()(
			// strip club prefix and load configs
			serv.StripClub()(
				// put form values into the context
				serv.formToContext()(
					// serve routes
					router))),
		ErrorLog:     serv.logger,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  15 * time.Second,
	}

	done := make(chan bool)
	quit := make(chan os.Signal, 1)
	// push into quit channel on os interrupts
	signal.Notify(quit, os.Interrupt)

	go func() {
		// wait until quit channel has a message
		<-quit
		serv.logger.Println("server is shutting down...")

		// mark server as unhealthy
		serv.healthMutex.Lock()
		serv.isHealthy = false
		serv.healthMutex.Unlock()

		// give server 30 seconds to shutdown
		ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
		defer cancel()

		// because server is shutting down stop http keepalives
		server.SetKeepAlivesEnabled(false)
		if err := server.Shutdown(ctx); err != nil {
			serv.logger.Printf("could not shut server down gracefully: %s\n", err.Error())
		}
		// close done channel to trigger final shutdown code in parent function
		close(done)
	}()

	serv.logger.Printf("server is ready to handle requests at %s\n", serv.Port)
	serv.healthMutex.Lock()
	serv.isHealthy = true
	serv.healthMutex.Unlock()
	if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		serv.logger.Printf("could not listen on %s: %s\n", serv.Port, err.Error())
		os.Exit(1)
	}

	// wait for done channel to log that server is stopped
	<-done
	serv.logger.Println("server stopped")

}
