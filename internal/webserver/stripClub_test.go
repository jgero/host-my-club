package webserver_test

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/jgero/host-my-club/internal/constants"
	"gitlab.com/jgero/host-my-club/internal/content"
	"gitlab.com/jgero/host-my-club/internal/webserver"
)

type MockConfigDto struct {
	ClubId string
}

func (dto *MockConfigDto) LoadConfigToContext(ctx context.Context) (context.Context, error) {
	dto.ClubId = ctx.Value(constants.ClubIdKey).(string)
	if dto.ClubId == "system" || dto.ClubId == "testclub" {
		cfg := content.ClubConfig{
			Id:          "testclub",
			Name:        "Club of united testers",
			Shorthand:   "test",
			CC:          "en",
			Description: "We should always test more but we are trying our best.",
			Palette: content.ColorPalette{
				Primary:     "#FF0000",
				Accent:      "#00FF00",
				Sheet:       "#FFFFFF",
				Background:  "#CCCCCC",
				Font:        "#000000",
				FontPrimary: "#FFFFFF",
				FontAccent:  "#FFFFFF",
			},
		}
		ctx = context.WithValue(ctx, constants.ClubConfigKey, cfg)
		return ctx, nil
	}
	return ctx, &content.ConfigMissingError{ClubId: dto.ClubId}
}
func (dto MockConfigDto) CreateNewClub(ctx context.Context, cfg content.ClubConfig, email string, password string, username string) (string, error) {
	return "", fmt.Errorf("club could not be created")
}
func (dto MockConfigDto) UpdateClubConfig(ctx context.Context, cfg content.ClubConfig) error {
	return nil
}

func (dto MockConfigDto) DeleteClub(ctx context.Context, clubid string) error {
	return nil
}

type testcase struct {
	name            string
	route           string
	wantClubId      string
	wantPathContext string
}

func TestValidRouteHandling(t *testing.T) {
	tests := []testcase{
		{name: "create club route", route: "/" + constants.CreateClubRoute, wantClubId: constants.SystemClubName, wantPathContext: "/" + constants.CreateClubRoute},
		{name: "welcome route", route: "/" + constants.WelcomeRoute, wantClubId: constants.SystemClubName, wantPathContext: "/" + constants.WelcomeRoute},
		{name: "club login page", route: fmt.Sprintf("/%s/%s", "testclub", constants.LoginRoute), wantClubId: "testclub", wantPathContext: "/" + constants.LoginRoute},
		{
			name:            "rename user endpoint",
			route:           fmt.Sprintf("/%s/%s/%s/%s", "testclub", constants.UserRoute, "4993bc78-3fd0-11ec-abbf-9ed65005a454", constants.RenameRoute),
			wantClubId:      "testclub",
			wantPathContext: fmt.Sprintf("/%s/%s/%s", constants.UserRoute, "4993bc78-3fd0-11ec-abbf-9ed65005a454", constants.RenameRoute),
		},
	}

	for _, tc := range tests {
		mockDto := &MockConfigDto{}
		server := webserver.Webserver{
			Port:      ":8080",
			ConfigDto: mockDto,
		}
		server.Init()
		verifyHandler := http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
			pathContext := r.Context().Value(constants.RequestPathKey).(string)
			if pathContext != tc.wantPathContext {
				t.Errorf("expected path in context to be '%s' but is '%s'", tc.wantPathContext, pathContext)
			}
		})

		handlerToTest := server.StripClub()(verifyHandler)
		req := httptest.NewRequest("GET", tc.route, nil)
		handlerToTest.ServeHTTP(httptest.NewRecorder(), req)

		if mockDto.ClubId != tc.wantClubId {
			t.Errorf("expected config for clubid '%s' to be loaded for route: %s:'%s', but it was '%s'", tc.wantClubId, tc.name, tc.route, mockDto.ClubId)
		}
	}
}

func TestInvalidRoutes(t *testing.T) {
	tests := []string{
		"/asdf",
		"/testclub/asdf",
		"/testclub/user/asdf",
		"/testclub/user/4993bc78-3fd0-11ec-abbf-9ed65005a454/asdf",
	}

	for _, tc := range tests {
		mockDto := &MockConfigDto{}
		server := webserver.Webserver{
			Port:      ":8080",
			ConfigDto: mockDto,
		}
		server.Init()
		verifyHandler := http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
			// check if all values are set correctly by the middleware
		})

		handlerToTest := server.StripClub()(verifyHandler)
		req := httptest.NewRequest("GET", tc, nil)
		recorder := httptest.NewRecorder()
		handlerToTest.ServeHTTP(recorder, req)

		if recorder.Code != http.StatusSeeOther || recorder.Header().Get("Location") != fmt.Sprintf("/%s/%d", constants.ErrorRoute, http.StatusNotFound) {
			t.Errorf("expected redirect to error page on route '%s' but got code '%d' with Location '%s'", tc, recorder.Code, recorder.Header().Get("Location"))
		}
	}
}
