package webserver_test

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/jgero/host-my-club/internal/constants"
	"gitlab.com/jgero/host-my-club/internal/content"
	"gitlab.com/jgero/host-my-club/internal/webserver"
)

const (
	testUserid = "testid"
	testClubid = "testclub"
	cookieName = "token"
)

type MockJwtConverter struct {
}

func (conv MockJwtConverter) GetClaimsFromToken(tokenString string) (*webserver.JwtClaims, error) {
	return &webserver.JwtClaims{
		Userid:   tokenString,
		IsAdmin:  false,
		IsEditor: true,
	}, nil
}
func (conv MockJwtConverter) BuildTokenFromClaims(claims webserver.JwtClaims) (string, error) {
	if !claims.IsAdmin && claims.IsEditor {
		return claims.Userid, nil
	}
	return "", fmt.Errorf("expected admin to be false (is %v) and editor to be true (is %v)", claims.IsAdmin, claims.IsEditor)
}

type MockUserAuthDto struct {
}

func (dto MockUserAuthDto) GetUserByEmail(context.Context, string) (*content.UserByEmail, error) {
	return &content.UserByEmail{
		Email:  "asdf@test.de",
		Userid: testUserid,
	}, nil
}
func (dto MockUserAuthDto) VerifyPassword(context.Context, string, string) (bool, error) {
	// all users and passwords are ok
	return true, nil
}
func (dto MockUserAuthDto) GetUserById(context.Context, string) (*content.UserById, error) {
	return &content.UserById{
		Username: "testuser",
		Userid:   testUserid,
		Email:    "does@not.matter",
		Groups:   []string{"banana", "editor"},
		IsAdmin:  false,
		IsEditor: true,
	}, nil
}

func TestCookieOnLogin(t *testing.T) {
	dto := MockUserAuthDto{}
	converter := MockJwtConverter{}
	server := webserver.Webserver{}
	server.Init()

	verifyHandler := http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		// nothing to verify in the following middleware
	})
	handlerToTest := server.Authentication(converter, dto)(verifyHandler)

	// prepare request
	req := httptest.NewRequest("POST", "/"+constants.LoginRoute, nil)
	ctx := req.Context()
	ctx = context.WithValue(ctx, "email", "does@neih.matter")
	ctx = context.WithValue(ctx, "password", "12341234")
	ctx = context.WithValue(ctx, constants.ClubIdKey, testClubid)
	ctx = context.WithValue(ctx, constants.RequestPathKey, fmt.Sprintf("/%s", constants.LoginRoute))
	recorder := httptest.NewRecorder()
	handlerToTest.ServeHTTP(recorder, req.WithContext(ctx))

	// check response
	if recorder.Result().StatusCode != http.StatusSeeOther {
		t.Errorf("expected statuscode to be %d but it is %d", http.StatusSeeOther, recorder.Result().StatusCode)
	}
	expectedRedirectLocation := fmt.Sprintf("/%s/%s/%s", testClubid, constants.UserRoute, testUserid)
	if recorder.Header().Get("Location") != expectedRedirectLocation {
		t.Errorf("expected redirect loation to be %s but it is %s", expectedRedirectLocation, recorder.Header().Get("Location"))
	}
	if len(recorder.Result().Cookies()) != 1 {
		t.Errorf("expected 1 cookie to be set but there are %d", len(recorder.Result().Cookies()))
		t.FailNow()
	}
	cookie := recorder.Result().Cookies()[0]
	if cookie.Name != "token" {
		t.Errorf("expected cookie name to be %s but it was %s", "token", cookie.Name)
	}
	if cookie.Value != testUserid {
		t.Errorf("expected cookie value to be %s but it was %s", testUserid, cookie.Value)
	}
	expectedCookiePath := fmt.Sprintf("/%s", testClubid)
	if cookie.Path != expectedCookiePath {
		t.Errorf("expected cookie path to be %s but it was %s", expectedCookiePath, cookie.Path)
	}
}

func TestCookieOnLogout(t *testing.T) {
	dto := MockUserAuthDto{}
	converter := MockJwtConverter{}
	server := webserver.Webserver{}
	server.Init()

	verifyHandler := http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		// nothing to verify in the following middleware
	})
	handlerToTest := server.Authentication(converter, dto)(verifyHandler)

	// prepare request
	req := httptest.NewRequest("POST", "/"+constants.LogoutRoute, nil)
	ctx := req.Context()
	ctx = context.WithValue(ctx, constants.ClubIdKey, testClubid)
	ctx = context.WithValue(ctx, constants.RequestPathKey, fmt.Sprintf("/%s", constants.LogoutRoute))
	req.AddCookie(&http.Cookie{Name: "token", Value: testUserid})
	recorder := httptest.NewRecorder()
	handlerToTest.ServeHTTP(recorder, req.WithContext(ctx))

	// check response
	if recorder.Result().StatusCode != http.StatusSeeOther {
		t.Errorf("expected statuscode to be %d but it is %d", http.StatusSeeOther, recorder.Result().StatusCode)
	}
	expectedRedirectLocation := fmt.Sprintf("/%s", testClubid)
	if recorder.Header().Get("Location") != expectedRedirectLocation {
		t.Errorf("expected redirect loation to be %s but it is %s", expectedRedirectLocation, recorder.Header().Get("Location"))
	}
	if len(recorder.Result().Cookies()) != 1 {
		t.Errorf("expected 1 cookie to be set but there are %d", len(recorder.Result().Cookies()))
		t.FailNow()
	}
	cookie := recorder.Result().Cookies()[0]
	if cookie.Name != "token" {
		t.Errorf("expected cookie name to be %s but it was %s", "token", cookie.Name)
	}
	if cookie.MaxAge != -1 {
		t.Errorf("expected cookie maxage to be %d but it was %d", -1, cookie.MaxAge)
	}
	expectedCookiePath := fmt.Sprintf("/%s", testClubid)
	if cookie.Path != expectedCookiePath {
		t.Errorf("expected cookie path to be %s but it was %s", expectedCookiePath, cookie.Path)
	}
}

func TestContextOnRequestWithCookie(t *testing.T) {
	dto := MockUserAuthDto{}
	converter := MockJwtConverter{}
	server := webserver.Webserver{}
	server.Init()

	verifyHandler := http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		// check context values
		if userid, ok := r.Context().Value(constants.UseridKey).(string); !ok {
			t.Errorf("expected context to have userid that is a string")
		} else if userid != testUserid {
			t.Errorf("expected userid to be %s but it is %s", testUserid, userid)
		}
		if isAdmin, ok := r.Context().Value(constants.IsAdminKey).(bool); !ok {
			t.Errorf("expected context to have isAdmin that is a bool")
		} else if isAdmin {
			t.Errorf("expected isAdmin to be false but it is true")
		}
		if isEditor, ok := r.Context().Value(constants.IsEditorKey).(bool); !ok {
			t.Errorf("expected context to have isEditor that is a bool")
		} else if !isEditor {
			t.Errorf("expected isEditor to be true but it is false")
		}
	})
	handlerToTest := server.Authentication(converter, dto)(verifyHandler)

	// prepare request
	req := httptest.NewRequest("GET", "/"+testClubid, nil)
	ctx := req.Context()
	ctx = context.WithValue(ctx, constants.ClubIdKey, testClubid)
	ctx = context.WithValue(ctx, constants.RequestPathKey, "/")
	req.AddCookie(&http.Cookie{Name: "token", Value: testUserid})
	recorder := httptest.NewRecorder()
	handlerToTest.ServeHTTP(recorder, req.WithContext(ctx))

	// check response
	if recorder.Result().StatusCode != http.StatusOK {
		t.Errorf("expected statuscode to be %d but it is %d", http.StatusSeeOther, recorder.Result().StatusCode)
	}
}
