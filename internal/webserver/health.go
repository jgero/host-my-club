package webserver

import "net/http"

// endpoint for health check
func (serv *Webserver) healthz() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		serv.healthMutex.Lock()
		healthStatusHealthy := serv.isHealthy
		serv.healthMutex.Unlock()
		if healthStatusHealthy {
			w.WriteHeader(http.StatusNoContent)
			return
		}
		w.WriteHeader(http.StatusServiceUnavailable)
	})
}
