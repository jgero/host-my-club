package endpoint

import (
	"context"
	"fmt"
	"regexp"

	"gitlab.com/jgero/host-my-club/internal/constants"
)

type DeleteClubDto interface {
	DeleteClub(context.Context, string) error
}

type DeleteClubHandler struct {
	ConfigDto DeleteClubDto
}

func (dc DeleteClubHandler) PathMatcher() *regexp.Regexp {
	return regexp.MustCompile(fmt.Sprintf("^/%s$", constants.DeleteRoute))
}

func (dc DeleteClubHandler) CheckAccess(ctx context.Context) error {
	_, ok := ctx.Value(constants.UseridKey).(string)
	if !ok {
		return &UnauthorizedError{Reason: "no user logged in", DeniedResourceName: "delete club"}
	}
	isAdmin, ok := ctx.Value(constants.IsAdminKey).(bool)
	if !ok || !isAdmin {
		return &UnauthorizedError{Reason: "user is no admin", DeniedResourceName: "delete club"}
	}
	return nil
}

func (dc DeleteClubHandler) HandleRequest(ctx context.Context) (string, error) {
	var ok bool

	clubId, ok := ctx.Value(constants.ClubIdKey).(string)
	if !ok {
		return "", fmt.Errorf("club id missing in context")
	}

	if err := dc.ConfigDto.DeleteClub(ctx, clubId); err != nil {
		return "", fmt.Errorf("error when executing query: %s", err)
	}
	return "/", nil
}

func (dc DeleteClubHandler) GetAuthAction() AuthAction {
	return LogoutOnSuccess
}
