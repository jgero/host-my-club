package endpoint

import (
	"context"
	"fmt"
	"regexp"

	"gitlab.com/jgero/host-my-club/internal/constants"
	"gitlab.com/jgero/host-my-club/internal/content"
)

type RegisterHandler struct {
	UserDto content.UserDto
}

func (rh RegisterHandler) PathMatcher() *regexp.Regexp {
	return regexp.MustCompile(fmt.Sprintf("^/%s$", constants.RegisterRoute))
}

// access only when no user is logged in
func (rh RegisterHandler) CheckAccess(ctx context.Context) error {
	userid := ctx.Value(constants.UseridKey)
	if userid != nil {
		return fmt.Errorf("userid is %s -> access denied", userid)
	}
	return nil
}

func (rh RegisterHandler) HandleRequest(ctx context.Context) (string, error) {
	var (
		email, password, username, clubid string
		ok                                bool
	)

	if username, ok = ctx.Value("username").(string); !ok {
		return "", &BadRequestError{Reason: "username missing in context"}
	}
	if email, ok = ctx.Value("email").(string); !ok {
		return "", &BadRequestError{Reason: "email missing in context"}
	}
	if password, ok = ctx.Value("password").(string); !ok {
		return "", &BadRequestError{Reason: "password missing in context"}
	}
	if clubid, ok = ctx.Value(constants.ClubIdKey).(string); !ok {
		return "", &BadRequestError{Reason: "clubid missing in context"}
	}
	if email == "" || password == "" || username == "" {
		return "", &BadRequestError{Reason: fmt.Sprintf("email: '%s', username: '%s' or password are empty", email, username)}
	}

	if _, err := rh.UserDto.CreateNewUser(ctx, email, password, username); err != nil {
		if _, ok := err.(*content.EmailTakenError); ok {
			return "", &ConflictError{Reason: "email is already taken", Redirect: fmt.Sprintf("/auth/register?emailTaken=true")}
		} else {
			return "", fmt.Errorf("could not create and store user in database: %s", err)
		}
	}

	return fmt.Sprintf("/%s/%s", clubid, constants.LoginRoute), nil
}

func (rh RegisterHandler) GetAuthAction() AuthAction {
	return NoAction
}
