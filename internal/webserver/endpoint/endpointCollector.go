package endpoint

import (
	"context"
	"fmt"
	"io"
	"regexp"

	"gitlab.com/jgero/host-my-club/internal/constants"
	"gitlab.com/jgero/host-my-club/internal/content"
)

type EndpointCollector struct {
	UserDto   content.UserDto
	ConfigDto content.ConfigDto
	routes    []EndpointHandler
}

type AuthAction int

const (
	LogoutOnSuccess AuthAction = iota
	LogoutOnError
	LogoutAlways
	NoAction
)

type EndpointHandler interface {
	// ran against the request path to determine which endpoint to use
	PathMatcher() *regexp.Regexp
	// check if access is allowed
	CheckAccess(ctx context.Context) error
	// handles the request
	HandleRequest(ctx context.Context) (string, error)
	GetAuthAction() AuthAction
}

type UnauthorizedError struct {
	Reason             string
	DeniedResourceName string
}

func (e *UnauthorizedError) Error() string {
	return fmt.Sprintf("request it not authorized to access %s because %s", e.DeniedResourceName, e.Reason)
}

type NoEndpointRouteMatched struct {
	UnmatchedPath string
}

func (e *NoEndpointRouteMatched) Error() string {
	return fmt.Sprintf("no route in the endpoint collector matched the path '%s'", e.UnmatchedPath)
}

type BadRequestError struct {
	Reason   string
	Redirect string
}

func (e *BadRequestError) Error() string {
	return fmt.Sprintf("request has invalid data: %s", e.Reason)
}

type ConflictError struct {
	Reason   string
	Redirect string
}

func (e *ConflictError) Error() string {
	return fmt.Sprintf("request causes conflict: %s", e.Reason)
}

func (ec *EndpointCollector) Init() {
	ec.routes = []EndpointHandler{
		RegisterHandler{
			UserDto: ec.UserDto,
		},
		RenameUserHandler{
			UserDto: ec.UserDto,
		},
		UpdateUserGroupsHandler{
			UserDto: ec.UserDto,
		},
		CreateClubHandler{
			ConfigDto: ec.ConfigDto,
		},
		DeleteUserHandler{
			UserDto: ec.UserDto,
		},
		DeleteClubHandler{
			ConfigDto: ec.ConfigDto,
		},
		UpdateClubHandler{
			ConfigDto: ec.ConfigDto,
		},
	}
}

// handle the request and return the route for redirection
func (ec EndpointCollector) HandleRequest(ctx context.Context, w io.Writer) (string, AuthAction, error) {
	path, ok := ctx.Value(constants.RequestPathKey).(string)
	if !ok {
		return "", NoAction, fmt.Errorf("request path is missing in context")
	}
	for _, route := range ec.routes {
		if !route.PathMatcher().MatchString(path) {
			continue
		}
		if err := route.CheckAccess(ctx); err != nil {
			return "", route.GetAuthAction(), err
		}
		if redirect, err := route.HandleRequest(ctx); err == nil {
			return redirect, route.GetAuthAction(), nil
		} else {
			return "", route.GetAuthAction(), err
		}
	}
	return "", NoAction, &NoEndpointRouteMatched{UnmatchedPath: path}
}
