package endpoint

import (
	"context"
	"fmt"
	"regexp"

	"gitlab.com/jgero/host-my-club/internal/constants"
	"gitlab.com/jgero/host-my-club/internal/content"
)

type CreateClubHandler struct {
	ConfigDto content.ConfigDto
}

func (cch CreateClubHandler) PathMatcher() *regexp.Regexp {
	return regexp.MustCompile(fmt.Sprintf("^/%s$", constants.CreateClubRoute))
}

func (cch CreateClubHandler) CheckAccess(ctx context.Context) error {
	return nil
}

func (cch CreateClubHandler) HandleRequest(ctx context.Context) (string, error) {
	cfg := content.ClubConfig{}
	var (
		username, email, password string
		ok                        bool
	)

	// TODO: check all these values somehow (patterns or something)
	if cfg.Name, ok = ctx.Value("fullname").(string); !ok {
		return "", &BadRequestError{Reason: "full club name missing in context"}
	}
	if cfg.Shorthand, ok = ctx.Value("shorthand").(string); !ok {
		return "", &BadRequestError{Reason: "full club shorthand missing in context"}
	}
	if cfg.Id, ok = ctx.Value("clubid").(string); !ok {
		return "", &BadRequestError{Reason: "club id missing in context"}
	}
	if cfg.Description, ok = ctx.Value("description").(string); !ok {
		return "", &BadRequestError{Reason: "club description missing in context"}
	}
	if cfg.CC, ok = ctx.Value("language").(string); !ok {
		return "", &BadRequestError{Reason: "club language missing in context"}
	}

	if cfg.Palette.Primary, ok = ctx.Value("primaryColor").(string); !ok {
		return "", &BadRequestError{Reason: "club primary color missing in context"}
	}
	if cfg.Palette.FontPrimary, ok = ctx.Value("primaryColorFont").(string); !ok {
		return "", &BadRequestError{Reason: "club primary font color missing in context"}
	}
	if cfg.Palette.Accent, ok = ctx.Value("accentColor").(string); !ok {
		return "", &BadRequestError{Reason: "club accent color missing in context"}
	}
	if cfg.Palette.FontAccent, ok = ctx.Value("accentColorFont").(string); !ok {
		return "", &BadRequestError{Reason: "club accent font color missing in context"}
	}
	if cfg.Palette.Font, ok = ctx.Value("fontColor").(string); !ok {
		return "", &BadRequestError{Reason: "club font color missing in context"}
	}
	if cfg.Palette.Background, ok = ctx.Value("backgroundColor").(string); !ok {
		return "", &BadRequestError{Reason: "club background color missing in context"}
	}
	if cfg.Palette.Sheet, ok = ctx.Value("sheetColor").(string); !ok {
		return "", &BadRequestError{Reason: "club sheet color missing in context"}
	}

	if username, ok = ctx.Value("username").(string); !ok {
		return "", &BadRequestError{Reason: "username missing in context"}
	}
	if email, ok = ctx.Value("email").(string); !ok {
		return "", &BadRequestError{Reason: "email missing in context"}
	}
	if password, ok = ctx.Value("password").(string); !ok {
		return "", &BadRequestError{Reason: "password missing in context"}
	}
	if email == "" || password == "" || username == "" {
		return "", &BadRequestError{Reason: fmt.Sprintf("email: '%s', username: '%s' or password are empty", email, username)}
	}

	if _, err := cch.ConfigDto.CreateNewClub(ctx, cfg, email, password, username); err != nil {
		return "", fmt.Errorf("could not create and store club or admin user in database: %s", err)
	}
	return fmt.Sprintf("/%s/%s", cfg.Id, constants.LoginRoute), nil
}

func (cch CreateClubHandler) GetAuthAction() AuthAction {
	return NoAction
}
