package endpoint

import (
	"context"
	"fmt"
	"regexp"
	"strings"

	"gitlab.com/jgero/host-my-club/internal/constants"
	"gitlab.com/jgero/host-my-club/internal/content"
)

type UpdateUserGroupsHandler struct {
	UserDto content.UserDto
}

func (ug UpdateUserGroupsHandler) PathMatcher() *regexp.Regexp {
	return regexp.MustCompile(fmt.Sprintf("^/%s/%s/%s$", constants.UserRoute, constants.UuidRegexpString(), constants.GroupsRoute))
}

// check if user is admin
func (ug UpdateUserGroupsHandler) CheckAccess(ctx context.Context) error {
	isAdmin, ok := ctx.Value(constants.IsAdminKey).(bool)
	if !ok {
		return fmt.Errorf("isAdmin value is not in context")
	}
	if !isAdmin {
		return &UnauthorizedError{Reason: "user is not an admin", DeniedResourceName: "update user groups"}
	}
	return nil
}

func (ug UpdateUserGroupsHandler) HandleRequest(ctx context.Context) (string, error) {
	newGroups, ok := ctx.Value("groups").(string)
	if !ok {
		return "", &BadRequestError{Reason: "new groups form entry missing"}
	}
	clubId, ok := ctx.Value(constants.ClubIdKey).(string)
	if !ok {
		return "", fmt.Errorf("club name missing in context")
	}

	if !regexp.MustCompile("^(([A-Za-zäöüÄÖÜß]{2,25}([A-Za-zäöüÄÖÜß]{2,25})?,)*([A-Za-zäöüÄÖÜß]{2,25}([A-Za-zäöüÄÖÜß]{2,25})?))?$").MatchString(newGroups) {
		return "", &BadRequestError{Reason: "new groups form entry is invalid"}
	}

	var path string
	path, ok = ctx.Value(constants.RequestPathKey).(string)
	if !ok {
		return "", fmt.Errorf("request path is missing in context")
	}

	targetUserid := strings.Split(path, "/")[2]
	adminUserid, ok := ctx.Value(constants.UseridKey).(string)
	if !ok {
		return "", fmt.Errorf("admin userid missing in context")
	}

	groups := []string{}
	if newGroups != "" {
		groups = strings.Split(newGroups, ",")
	}
	if err := ug.UserDto.UpdateUserGroups(ctx, targetUserid, groups); err != nil {
		return "", fmt.Errorf("executing query failed: %s", err)
	}
	return fmt.Sprintf("/%s/%s/%s#%s", clubId, constants.UserRoute, adminUserid, targetUserid), nil
}

func (ug UpdateUserGroupsHandler) GetAuthAction() AuthAction {
	return NoAction
}
