package endpoint

import (
	"context"
	"fmt"
	"regexp"
	"strings"

	"gitlab.com/jgero/host-my-club/internal/constants"
	"gitlab.com/jgero/host-my-club/internal/content"
)

type RenameUserHandler struct {
	UserDto content.UserDto
}

func (ru RenameUserHandler) PathMatcher() *regexp.Regexp {
	return regexp.MustCompile(fmt.Sprintf("^/%s/%s/%s$", constants.UserRoute, constants.UuidRegexpString(), constants.RenameRoute))
}

func (ru RenameUserHandler) CheckAccess(ctx context.Context) error {
	userid, ok := ctx.Value(constants.UseridKey).(string)
	if !ok {
		return &UnauthorizedError{Reason: "no user logged in", DeniedResourceName: "rename user"}
	}
	var path string
	path, ok = ctx.Value(constants.RequestPathKey).(string)
	if !ok {
		return fmt.Errorf("request path is not in context")
	}

	useridInPath := strings.Split(path, "/")[2]
	if useridInPath != userid {
		return &UnauthorizedError{Reason: fmt.Sprintf("authenticated userid %s is not the same as userid %s from request path", userid, useridInPath), DeniedResourceName: "rename user"}
	}
	return nil
}

func (ru RenameUserHandler) HandleRequest(ctx context.Context) (string, error) {
	var newUsername string
	var ok bool

	if newUsername, ok = ctx.Value("username").(string); !ok {
		return "", &BadRequestError{Reason: "username form entry missing"}
	}
	clubId, ok := ctx.Value(constants.ClubIdKey).(string)
	if !ok {
		return "", fmt.Errorf("club name missing in context")
	}

	if !regexp.MustCompile("^[a-zA-ZäöüÄÖÜß -]{5,45}$").MatchString(newUsername) {
		return "", &BadRequestError{Reason: "username missing or invalid format"}
	}

	var path string
	path, ok = ctx.Value(constants.RequestPathKey).(string)
	if !ok {
		return "", fmt.Errorf("request path is not in context")
	}
	useridFromPath := strings.Split(path, "/")[2]

	if err := ru.UserDto.UpdateUsername(ctx, useridFromPath, newUsername); err != nil {
		return "", fmt.Errorf("error when executing query: %s", err)
	}
	return fmt.Sprintf("/%s/%s/%s", clubId, constants.UserRoute, useridFromPath), nil
}

func (ru RenameUserHandler) GetAuthAction() AuthAction {
	return NoAction
}
