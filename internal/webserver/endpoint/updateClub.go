package endpoint

import (
	"context"
	"fmt"
	"regexp"

	"gitlab.com/jgero/host-my-club/internal/constants"
	"gitlab.com/jgero/host-my-club/internal/content"
)

type UpdateClubDto interface {
	UpdateClubConfig(context.Context, content.ClubConfig) error
}

type UpdateClubHandler struct {
	ConfigDto UpdateClubDto
}

func (ech UpdateClubHandler) PathMatcher() *regexp.Regexp {
	return regexp.MustCompile(fmt.Sprintf("^/%s$", constants.UpdateRoute))
}

func (ech UpdateClubHandler) CheckAccess(ctx context.Context) error {
	_, ok := ctx.Value(constants.UseridKey).(string)
	if !ok {
		return &UnauthorizedError{Reason: "no user logged in", DeniedResourceName: "update club"}
	}
	isAdmin, ok := ctx.Value(constants.IsAdminKey).(bool)
	if !ok || !isAdmin {
		return &UnauthorizedError{Reason: "user is no admin", DeniedResourceName: "update club"}
	}
	return nil
}

func (ech UpdateClubHandler) HandleRequest(ctx context.Context) (string, error) {
	cfg, ok := ctx.Value(constants.ClubConfigKey).(content.ClubConfig)
	if !ok {
		return "", fmt.Errorf("club config missing in context")
	}
	userid, ok := ctx.Value(constants.UseridKey).(string)
	if !ok {
		return "", fmt.Errorf("userid missing in context")
	}

	// TODO: check all these values somehow (patterns or something)
	if cfg.Name, ok = ctx.Value("fullname").(string); !ok {
		return "", &BadRequestError{Reason: "full club name missing in context"}
	}
	if cfg.Shorthand, ok = ctx.Value("shorthand").(string); !ok {
		return "", &BadRequestError{Reason: "full club shorthand missing in context"}
	}
	if cfg.Description, ok = ctx.Value("description").(string); !ok {
		return "", &BadRequestError{Reason: "club description missing in context"}
	}

	if cfg.Palette.Primary, ok = ctx.Value("primaryColor").(string); !ok {
		return "", &BadRequestError{Reason: "club primary color missing in context"}
	}
	if cfg.Palette.FontPrimary, ok = ctx.Value("primaryColorFont").(string); !ok {
		return "", &BadRequestError{Reason: "club primary font color missing in context"}
	}
	if cfg.Palette.Accent, ok = ctx.Value("accentColor").(string); !ok {
		return "", &BadRequestError{Reason: "club accent color missing in context"}
	}
	if cfg.Palette.FontAccent, ok = ctx.Value("accentColorFont").(string); !ok {
		return "", &BadRequestError{Reason: "club accent font color missing in context"}
	}
	if cfg.Palette.Font, ok = ctx.Value("fontColor").(string); !ok {
		return "", &BadRequestError{Reason: "club font color missing in context"}
	}
	if cfg.Palette.Background, ok = ctx.Value("backgroundColor").(string); !ok {
		return "", &BadRequestError{Reason: "club background color missing in context"}
	}
	if cfg.Palette.Sheet, ok = ctx.Value("sheetColor").(string); !ok {
		return "", &BadRequestError{Reason: "club sheet color missing in context"}
	}

	if err := ech.ConfigDto.UpdateClubConfig(ctx, cfg); err != nil {
		return "", fmt.Errorf("could not update club in database: %s", err)
	}
	return fmt.Sprintf("/%s/%s/%s", cfg.Id, constants.UserRoute, userid), nil
}

func (ech UpdateClubHandler) GetAuthAction() AuthAction {
	return NoAction
}
