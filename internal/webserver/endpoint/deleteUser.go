package endpoint

import (
	"context"
	"fmt"
	"regexp"
	"strings"

	"gitlab.com/jgero/host-my-club/internal/constants"
	"gitlab.com/jgero/host-my-club/internal/content"
)

type DeleteUserHandler struct {
	UserDto content.UserDto
}

func (du DeleteUserHandler) PathMatcher() *regexp.Regexp {
	return regexp.MustCompile(fmt.Sprintf("^/%s/%s/%s$", constants.UserRoute, constants.UuidRegexpString(), constants.DeleteRoute))
}

func (du DeleteUserHandler) CheckAccess(ctx context.Context) error {
	userid, ok := ctx.Value(constants.UseridKey).(string)
	if !ok {
		return &UnauthorizedError{Reason: "no user logged in", DeniedResourceName: "rename user"}
	}
	var path string
	path, ok = ctx.Value(constants.RequestPathKey).(string)
	if !ok {
		return fmt.Errorf("request path is not in context")
	}

	useridInPath := strings.Split(path, "/")[2]
	if useridInPath != userid {
		return &UnauthorizedError{Reason: fmt.Sprintf("authenticated userid %s is not the same as userid %s from request path", userid, useridInPath), DeniedResourceName: "rename user"}
	}
	return nil
}

func (du DeleteUserHandler) HandleRequest(ctx context.Context) (string, error) {
	var ok bool

	clubId, ok := ctx.Value(constants.ClubIdKey).(string)
	if !ok {
		return "", fmt.Errorf("club name missing in context")
	}

	var path string
	path, ok = ctx.Value(constants.RequestPathKey).(string)
	if !ok {
		return "", fmt.Errorf("request path is not in context")
	}
	useridFromPath := strings.Split(path, "/")[2]

	if err := du.UserDto.DeleteUser(ctx, useridFromPath); err != nil {
		return "", fmt.Errorf("error when executing query: %s", err)
	}
	return fmt.Sprintf("/%s", clubId), nil
}

func (du DeleteUserHandler) GetAuthAction() AuthAction {
	return LogoutOnSuccess
}
