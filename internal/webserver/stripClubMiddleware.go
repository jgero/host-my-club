package webserver

import (
	"context"
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/jgero/host-my-club/internal/constants"
	"gitlab.com/jgero/host-my-club/internal/content"
)

func (serv *Webserver) StripClub() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r.URL.Path == "/" {
				http.Redirect(w, r, "/"+constants.WelcomeRoute, http.StatusSeeOther)
			} else if strings.HasPrefix(r.URL.Path, "/"+constants.StaticRoute) {
				next.ServeHTTP(w, r)
			} else if constants.SystemRouteRegex().MatchString(r.URL.Path) {
				ctx := r.Context()
				ctx = context.WithValue(ctx, constants.RequestPathKey, r.URL.Path)
				// load system club
				ctx = context.WithValue(ctx, constants.ClubIdKey, constants.SystemClubName)
				newCtx, err := serv.ConfigDto.LoadConfigToContext(ctx)
				if err != nil {
					serv.logger.Println(err)
					http.Redirect(w, r, fmt.Sprintf("/%s/%d", constants.ErrorRoute, http.StatusInternalServerError), http.StatusSeeOther)
					return
				}
				cfg, ok := newCtx.Value(constants.ClubConfigKey).(content.ClubConfig)
				if !ok {
					serv.logger.Println("config in context is not okay")
					http.Redirect(w, r, fmt.Sprintf("/%s/%d", constants.ErrorRoute, http.StatusInternalServerError), http.StatusSeeOther)
					return
				}
				loc, err := constants.GetLocalesForCC(cfg.CC)
				if err != nil {
					serv.logger.Println(err)
					http.Redirect(w, r, fmt.Sprintf("/%s/%d", constants.ErrorRoute, http.StatusInternalServerError), http.StatusSeeOther)
					return
				}
				newCtx = context.WithValue(newCtx, constants.SystemLocalesKey, loc)
				next.ServeHTTP(w, r.WithContext(newCtx))
			} else if constants.ClubRouteRegex().MatchString(r.URL.Path) {
				clubId := constants.GetClubNameFromClubRoute(r.URL.Path)
				if clubId == constants.SystemClubName {
					serv.logger.Println("system club does not have routes")
					http.Redirect(w, r, fmt.Sprintf("/%s/%d", constants.ErrorRoute, http.StatusNotFound), http.StatusSeeOther)
					return
				}
				ctx := r.Context()
				ctx = context.WithValue(ctx, constants.ClubIdKey, clubId)
				trimmedRoute := strings.TrimPrefix(r.URL.Path, "/"+clubId)
				if trimmedRoute == "" {
					trimmedRoute = "/"
				}
				ctx = context.WithValue(ctx, constants.RequestPathKey, trimmedRoute)

				newCtx, err := serv.ConfigDto.LoadConfigToContext(ctx)
				if err != nil {
					if _, ok := err.(*content.ConfigMissingError); ok {
						serv.logger.Println(err)
						http.Redirect(w, r, fmt.Sprintf("/%s/%d", constants.ErrorRoute, http.StatusNotFound), http.StatusSeeOther)
						return
					} else {
						serv.logger.Println(err)
						http.Redirect(w, r, fmt.Sprintf("/%s/%d", constants.ErrorRoute, http.StatusInternalServerError), http.StatusSeeOther)
						return
					}
				}

				cfg, ok := newCtx.Value(constants.ClubConfigKey).(content.ClubConfig)
				if !ok {
					// this is actually an error because there should be a config even if the
					// non-club landingpage is loaded
					serv.logger.Println("club config in context is not okay")
					http.Redirect(w, r, fmt.Sprintf("/%s/%d", constants.ErrorRoute, http.StatusInternalServerError), http.StatusSeeOther)
					return
				}

				if loc, err := constants.GetLocalesForCC(cfg.CC); err != nil {
					serv.logger.Println("locales in context are not okay")
					http.Redirect(w, r, fmt.Sprintf("/%s/%d", constants.ErrorRoute, http.StatusInternalServerError), http.StatusSeeOther)
					return
				} else {
					newCtx = context.WithValue(newCtx, constants.SystemLocalesKey, loc)
				}

				next.ServeHTTP(w, r.WithContext(newCtx))
			} else {
				// TODO: this is only a warning, not a full error, user may be typing freely into
				// url bar
				serv.logger.Printf("no regexes matched %s\n", r.URL.Path)
				http.Redirect(w, r, fmt.Sprintf("/%s/%d", constants.ErrorRoute, http.StatusNotFound), http.StatusSeeOther)
			}
		})
	}
}
