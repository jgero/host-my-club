package webserver

import (
	"context"
	"net/http"
	"time"

	"github.com/google/uuid"
	"gitlab.com/jgero/host-my-club/internal/constants"
)

func getNextRequestId() string {
	return uuid.New().String()
}

// middleware to log all requests and give them an id
func (serv *Webserver) logging() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			startTime := time.Now()

			// give the request an id
			requestId := r.Header.Get("X-Request-Id")
			if requestId == "" {
				requestId = getNextRequestId()
			}
			ctx := context.WithValue(r.Context(), constants.RequestIDKey, requestId)
			w.Header().Set("X-Request-Id", requestId)

			// log after the request was served
			defer func() {
				serv.logger.Println(requestId, r.Method, r.URL.Path, time.Since(startTime).Milliseconds())
			}()
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}
