package webserver

import (
	"context"
	"fmt"
	"net/http"
	"regexp"

	"gitlab.com/jgero/host-my-club/internal/constants"
	"gitlab.com/jgero/host-my-club/internal/content"
)

type JwtConverter interface {
	GetClaimsFromToken(tokenString string) (*JwtClaims, error)
	BuildTokenFromClaims(claims JwtClaims) (string, error)
}

type UserAuthDto interface {
	GetUserByEmail(context.Context, string) (*content.UserByEmail, error)
	VerifyPassword(context.Context, string, string) (bool, error)
	GetUserById(context.Context, string) (*content.UserById, error)
}

// Authentication middleware
// In default cases it reads the JWT of the request and sets the context accordingly. For the login
// and logout routes it also sets the token and directly responds the request.
func (serv *Webserver) Authentication(conv JwtConverter, dto UserAuthDto) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {

			// read from context
			path, ok := r.Context().Value(constants.RequestPathKey).(string)
			if !ok {
				serv.logger.Println("request path not in context")
				http.Redirect(rw, r, fmt.Sprintf("/%s/%d", constants.ErrorRoute, http.StatusInternalServerError), http.StatusSeeOther)
				return
			}
			clubid, ok := r.Context().Value(constants.ClubIdKey).(string)
			if !ok {
				serv.logger.Println("club id not in context")
				http.Redirect(rw, r, fmt.Sprintf("/%s/%d", constants.ErrorRoute, http.StatusInternalServerError), http.StatusSeeOther)
				return
			}

			// login route: check credentilas and build new cookie -> redirect to user page
			if r.Method == "POST" && regexp.MustCompile(fmt.Sprintf("^/%s$", constants.LoginRoute)).MatchString(path) {
				userById, err := login(r.Context(), dto)
				if err != nil {
					serv.logger.Println(err)
					// TODO: show message for these params on login page
					switch e := err.(type) {
					case *content.UserMissingError:
						http.Redirect(rw, r, fmt.Sprintf("/%s/%s?noAccount=%s", clubid, constants.LoginRoute, e.Key), http.StatusSeeOther)
						return
					case *WrongPasswordError:
						http.Redirect(rw, r, fmt.Sprintf("/%s/%s?wrongPassword=true", clubid, constants.LoginRoute), http.StatusSeeOther)
						return
					case *BadRequestError:
						http.Redirect(rw, r, fmt.Sprintf("/%s/%s?badRequest=true", clubid, constants.LoginRoute), http.StatusSeeOther)
						return
					default:
						http.Redirect(rw, r, fmt.Sprintf("/%s/%d", constants.ErrorRoute, http.StatusInternalServerError), http.StatusSeeOther)
						return
					}
				}
				tokenString, err := conv.BuildTokenFromClaims(JwtClaims{
					Userid:   userById.Userid,
					IsAdmin:  userById.IsAdmin,
					IsEditor: userById.IsEditor,
				})
				if err != nil {
					serv.logger.Println(err)
					http.Redirect(rw, r, fmt.Sprintf("/%s/%d", constants.ErrorRoute, http.StatusInternalServerError), http.StatusSeeOther)
					return
				}
				http.SetCookie(rw, &http.Cookie{
					Name:     "token",
					Value:    tokenString,
					Path:     fmt.Sprintf("/%s", clubid),
					HttpOnly: true,
					MaxAge:   60 * 60 * 24 * 7,
				})
				http.Redirect(rw, r, fmt.Sprintf("/%s/%s/%s", clubid, constants.UserRoute, userById.Userid), http.StatusSeeOther)
				return
			}

			// logout route: remove cookie -> redirect to startpage
			if r.Method == "POST" && regexp.MustCompile(fmt.Sprintf("^/%s$", constants.LogoutRoute)).MatchString(path) {
				http.SetCookie(rw, &http.Cookie{
					Name:     "token",
					Value:    "",
					Path:     fmt.Sprintf("/%s", clubid),
					HttpOnly: true,
					MaxAge:   -1,
				})
				http.Redirect(rw, r, fmt.Sprintf("/%s", clubid), http.StatusSeeOther)
				return
			}

			// for every other route read cookie and set context
			tokenCookie, err := r.Cookie("token")
			if err != nil {
				// error here means no cookie -> just do nothing and go next
				next.ServeHTTP(rw, r)
				return
			}
			claims, err := conv.GetClaimsFromToken(tokenCookie.Value)
			if err != nil {
				serv.logger.Println(err)
				http.Redirect(rw, r, fmt.Sprintf("/%s/%d", constants.ErrorRoute, http.StatusInternalServerError), http.StatusSeeOther)
				return
			}
			ctx := r.Context()
			ctx = context.WithValue(ctx, constants.UseridKey, claims.Userid)
			ctx = context.WithValue(ctx, constants.IsAdminKey, claims.IsAdmin)
			ctx = context.WithValue(ctx, constants.IsEditorKey, claims.IsEditor)
			next.ServeHTTP(rw, r.WithContext(ctx))
			return
		})
	}
}

type BadRequestError struct {
	Reason string
}

func (e *BadRequestError) Error() string {
	return fmt.Sprintf("bad request: %s", e.Reason)
}

type WrongPasswordError struct{}

func (e *WrongPasswordError) Error() string {
	return fmt.Sprint("wrong password")
}

func login(ctx context.Context, dto UserAuthDto) (*content.UserById, error) {
	var (
		email, password, userid string
		ok                      = false
	)

	// get info from context
	if email, ok = ctx.Value("email").(string); !ok || email == "" {
		return nil, &BadRequestError{Reason: "email missing in context"}
	}
	if password, ok = ctx.Value("password").(string); !ok || password == "" {
		return nil, &BadRequestError{Reason: "password missing in context"}
	}

	// gather data from database
	if userByEmail, err := dto.GetUserByEmail(ctx, email); err != nil {
		return nil, err
	} else {
		userid = userByEmail.Userid
	}
	if isValid, err := dto.VerifyPassword(ctx, email, password); err != nil {
		return nil, err
	} else if !isValid {
		return nil, &WrongPasswordError{}
	}
	if userById, err := dto.GetUserById(ctx, userid); err != nil {
		return nil, err
	} else {
		return userById, nil
	}
}
