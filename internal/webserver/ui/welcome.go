package ui

import (
	"context"
	"fmt"
	"html/template"
	"regexp"

	"gitlab.com/jgero/host-my-club/internal/constants"
	"gitlab.com/jgero/host-my-club/internal/content"
)

type WelcomePage struct {
}

func (p WelcomePage) Name() string {
	return "easily"
}

func (p WelcomePage) LayoutTemplateName() string {
	return "layout_system.html"
}

func (p WelcomePage) PathMatcher() *regexp.Regexp {
	return regexp.MustCompile(fmt.Sprintf("^/%s$", constants.WelcomeRoute))
}

func (p WelcomePage) CheckAccess(ctx context.Context) error {
	return nil
}

func (p WelcomePage) TemplateData(ctx context.Context) (*TemplateDataValues, error) {
	cfg, ok := ctx.Value(constants.ClubConfigKey).(content.ClubConfig)
	if !ok {
		return nil, fmt.Errorf("club config missing in context")
	}
	locales, ok := ctx.Value(constants.SystemLocalesKey).(constants.Locales)
	if !ok {
		return nil, fmt.Errorf("locales missing in context")
	}
	return &TemplateDataValues{
		MetaTags: MetaTags{
			Title:        fmt.Sprintf("%s | %s", locales.WelcomeLabel, locales.BrandName),
			Description:  fmt.Sprintf("%s | %s", locales.WelcomeLabel, locales.BrandName),
			CanonicalUrl: fmt.Sprintf("/%s", constants.WelcomeRoute),
			ImageUrl:     "/static/ClubIcon.webp",
		},
		Club:    cfg,
		Locales: locales,
	}, nil
}

func (p WelcomePage) FuncMap() *template.FuncMap {
	return nil
}

func (p WelcomePage) AdditionalTemplates() []string {
	return []string{}
}
