package ui

import (
	"context"
	"fmt"
	"html/template"
	"regexp"
	"strings"

	"gitlab.com/jgero/host-my-club/internal/constants"
	"gitlab.com/jgero/host-my-club/internal/content"
)

type UserPageUserDto interface {
	GetUserById(context.Context, string) (*content.UserById, error)
	GetAllUsers(context.Context) ([]content.UserById, error)
}

type UserPage struct {
	userDto UserPageUserDto
}

func (p UserPage) Name() string {
	return "user"
}

func (p UserPage) LayoutTemplateName() string {
	return "layout.html"
}

func (p UserPage) PathMatcher() *regexp.Regexp {
	return regexp.MustCompile(fmt.Sprintf("^/%s/%s$", constants.UserRoute, constants.UuidRegexpString()))
}

func (p UserPage) CheckAccess(ctx context.Context) error {
	// check if there is a userid and path in the context
	userid, ok := ctx.Value(constants.UseridKey).(string)
	if !ok {
		return &UnauthorizedError{Reason: "no user logged in or invalid userid", DeniedResourceName: "user profile"}
	}
	var requestPath string
	requestPath, ok = ctx.Value(constants.RequestPathKey).(string)
	if !ok {
		return fmt.Errorf("context does not contain valid request path key")
	}
	// check if user is trying to access own profile
	if !strings.Contains(requestPath, userid) {
		return &UnauthorizedError{Reason: "user is not allowed to view that profile", DeniedResourceName: "user profile"}
	}
	return nil
}

func (p UserPage) TemplateData(ctx context.Context) (*TemplateDataValues, error) {
	requestPath, ok := ctx.Value(constants.RequestPathKey).(string)
	if !ok {
		return nil, fmt.Errorf("no request path in context")
	}
	clubId, ok := ctx.Value(constants.ClubIdKey).(string)
	if !ok {
		return nil, fmt.Errorf("club name missing in context")
	}
	cfg, ok := ctx.Value(constants.ClubConfigKey).(content.ClubConfig)
	if !ok {
		return nil, fmt.Errorf("club config missing in context")
	}
	locales, ok := ctx.Value(constants.SystemLocalesKey).(constants.Locales)
	if !ok {
		return nil, fmt.Errorf("locales missing in context")
	}
	userid := strings.Split(requestPath, "/")[2]
	user, err := p.userDto.GetUserById(ctx, userid)
	if err != nil {
		return nil, err
	}
	allUsers, err := p.userDto.GetAllUsers(ctx)
	return &TemplateDataValues{
		MetaTags: MetaTags{
			Title:        fmt.Sprintf("%s - %s | %s", locales.ProfileTitle, cfg.Shorthand, locales.BrandName),
			Description:  fmt.Sprintf("%s - %s | %s", locales.ProfileTitle, cfg.Name, locales.BrandName),
			CanonicalUrl: fmt.Sprintf("/%s/%s/%s", clubId, constants.UserRoute, userid),
			ImageUrl:     "/static/ClubIcon.webp",
		},
		Club:    cfg,
		Locales: locales,
		Supplementary: map[string]interface{}{
			"Username": user.Username,
			"Userid":   userid,
			"Email":    user.Email,
			"Groups":   user.Groups,
			"IsAdmin":  user.IsAdmin,
			"IsEditor": user.IsEditor,
			"Users":    allUsers,
		},
	}, nil
}

func (p UserPage) FuncMap() *template.FuncMap {
	return &template.FuncMap{
		"join": func(groups []string) string {
			return strings.Join(groups, ",")
		},
	}
}

func (p UserPage) AdditionalTemplates() []string {
	return []string{}
}
