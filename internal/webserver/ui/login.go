package ui

import (
	"context"
	"fmt"
	"html/template"
	"regexp"

	"gitlab.com/jgero/host-my-club/internal/constants"
	"gitlab.com/jgero/host-my-club/internal/content"
)

type LoginPage struct {
}

func (p LoginPage) Name() string {
	return "login"
}

func (p LoginPage) LayoutTemplateName() string {
	return "layout.html"
}

func (p LoginPage) PathMatcher() *regexp.Regexp {
	return regexp.MustCompile(fmt.Sprintf("^/%s$", constants.LoginRoute))
}

func (p LoginPage) CheckAccess(ctx context.Context) error {
	return nil
}

func (p LoginPage) TemplateData(ctx context.Context) (*TemplateDataValues, error) {
	wrongPw := false
	if val, ok := ctx.Value("wrongPassword").(string); ok && val == "true" {
		wrongPw = true
	}
	clubId, ok := ctx.Value(constants.ClubIdKey).(string)
	if !ok {
		return nil, fmt.Errorf("club name missing in context")
	}
	cfg, ok := ctx.Value(constants.ClubConfigKey).(content.ClubConfig)
	if !ok {
		return nil, fmt.Errorf("club config missing in context")
	}
	locales, ok := ctx.Value(constants.SystemLocalesKey).(constants.Locales)
	if !ok {
		return nil, fmt.Errorf("locales missing in context")
	}
	return &TemplateDataValues{
		MetaTags: MetaTags{
			Title:        fmt.Sprintf("%s - %s | %s", locales.LoginTitle, cfg.Shorthand, locales.BrandName),
			Description:  fmt.Sprintf("%s - %s | %s", locales.LoginTitle, cfg.Name, locales.BrandName),
			CanonicalUrl: fmt.Sprintf("/%s/%s", clubId, constants.LoginRoute),
			ImageUrl:     "/static/ClubIcon.webp",
		},
		Club:    cfg,
		Locales: locales,
		Supplementary: map[string]interface{}{
			"WrongPassword": wrongPw,
		},
	}, nil
}

func (p LoginPage) FuncMap() *template.FuncMap {
	return nil
}

func (p LoginPage) AdditionalTemplates() []string {
	return []string{}
}
