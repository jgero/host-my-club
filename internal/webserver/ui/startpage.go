package ui

import (
	"context"
	"fmt"
	"html/template"
	"regexp"

	"gitlab.com/jgero/host-my-club/internal/constants"
	"gitlab.com/jgero/host-my-club/internal/content"
)

type StartPage struct {
}

func (p StartPage) Name() string {
	return "startpage"
}

func (p StartPage) LayoutTemplateName() string {
	return "layout.html"
}

func (p StartPage) PathMatcher() *regexp.Regexp {
	return regexp.MustCompile("^/$")
}

func (p StartPage) CheckAccess(ctx context.Context) error {
	return nil
}

func (p StartPage) TemplateData(ctx context.Context) (*TemplateDataValues, error) {
	cfg, ok := ctx.Value(constants.ClubConfigKey).(content.ClubConfig)
	if !ok {
		return nil, fmt.Errorf("club config missing in context")
	}
	locales, ok := ctx.Value(constants.SystemLocalesKey).(constants.Locales)
	if !ok {
		return nil, fmt.Errorf("locales missing in context")
	}
	clubid, ok := ctx.Value(constants.ClubIdKey).(string)
	if !ok {
		return nil, fmt.Errorf("club id missing in context")
	}
	userButtonLink := fmt.Sprintf("/%s/%s", clubid, constants.LoginRoute)
	userid, ok := ctx.Value(constants.UseridKey).(string)
	if ok {
		userButtonLink = fmt.Sprintf("/%s/%s/%s", clubid, constants.UserRoute, userid)
	}
	return &TemplateDataValues{
		MetaTags: MetaTags{
			Title:        fmt.Sprintf("%s | %s", cfg.Name, locales.BrandName),
			Description:  fmt.Sprintf("%s | %s", cfg.Name, locales.BrandName),
			CanonicalUrl: fmt.Sprintf("/%s", cfg.Id),
			ImageUrl:     "/static/ClubIcon.webp",
		},
		Club:    cfg,
		Locales: locales,
		Supplementary: map[string]interface{}{
			"UserButtonLink": userButtonLink,
		},
	}, nil
}

func (p StartPage) FuncMap() *template.FuncMap {
	return nil
}

func (p StartPage) AdditionalTemplates() []string {
	return []string{}
}
