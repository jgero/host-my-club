package ui

import (
	"context"
	"fmt"
	"html/template"
	"net/http"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/jgero/host-my-club/internal/constants"
	"gitlab.com/jgero/host-my-club/internal/content"
)

type ErrorPage struct {
}

func (e ErrorPage) Name() string {
	return "error"
}

func (e ErrorPage) LayoutTemplateName() string {
	return "layout_system.html"
}

func (e ErrorPage) PathMatcher() *regexp.Regexp {
	return regexp.MustCompile(fmt.Sprintf("^/%s/(%d|%d|%d)/?$", constants.ErrorRoute, http.StatusNotFound, http.StatusInternalServerError, http.StatusUnauthorized))
}

func (e ErrorPage) CheckAccess(ctx context.Context) error {
	return nil
}

func (e ErrorPage) TemplateData(ctx context.Context) (*TemplateDataValues, error) {
	var errorCode int
	if val, ok := ctx.Value(constants.RequestPathKey).(string); ok {
		var err error
		errorCode, err = strconv.Atoi(strings.Split(val, "/")[2])
		if err != nil {
			return nil, fmt.Errorf("could not extract error code from request path: %s", err)
		}
	} else {
		return nil, fmt.Errorf("could not read request path from context")
	}
	cfg, ok := ctx.Value(constants.ClubConfigKey).(content.ClubConfig)
	if !ok {
		return nil, fmt.Errorf("club config missing in context")
	}
	locales, ok := ctx.Value(constants.SystemLocalesKey).(constants.Locales)
	if !ok {
		return nil, fmt.Errorf("locales missing in context")
	}
	return &TemplateDataValues{
		MetaTags: MetaTags{
			Title:        fmt.Sprintf("%s - %s | %s", locales.ErrorTitle, cfg.Shorthand, locales.BrandName),
			Description:  fmt.Sprintf("%s - %s | %s ", locales.ErrorTitle, cfg.Name, locales.BrandName),
			CanonicalUrl: fmt.Sprintf("/%s/%d", constants.ErrorRoute, errorCode),
			ImageUrl:     "/static/ClubIcon.webp",
		},
		Club:    cfg,
		Locales: locales,
		Supplementary: map[string]interface{}{
			"ErrorCode": errorCode,
		},
	}, nil
}

func (e ErrorPage) FuncMap() *template.FuncMap {
	return nil
}

func (e ErrorPage) AdditionalTemplates() []string {
	return []string{}
}
