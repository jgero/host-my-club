package ui

import (
	"context"
	"fmt"
	"html/template"
	"regexp"

	"gitlab.com/jgero/host-my-club/internal/constants"
	"gitlab.com/jgero/host-my-club/internal/content"
)

type RegisterPage struct {
}

func (p RegisterPage) Name() string {
	return "register"
}

func (p RegisterPage) LayoutTemplateName() string {
	return "layout.html"
}

func (p RegisterPage) PathMatcher() *regexp.Regexp {
	return regexp.MustCompile(fmt.Sprintf("^/%s", constants.RegisterRoute))
}

func (p RegisterPage) CheckAccess(ctx context.Context) error {
	return nil
}

func (p RegisterPage) TemplateData(ctx context.Context) (*TemplateDataValues, error) {
	emailTaken := false
	if val, ok := ctx.Value("emailTaken").(string); ok && val == "true" {
		emailTaken = true
	}
	clubId, ok := ctx.Value(constants.ClubIdKey).(string)
	if !ok {
		return nil, fmt.Errorf("club name missing in context")
	}
	cfg, ok := ctx.Value(constants.ClubConfigKey).(content.ClubConfig)
	if !ok {
		return nil, fmt.Errorf("club config missing in context")
	}
	locales, ok := ctx.Value(constants.SystemLocalesKey).(constants.Locales)
	if !ok {
		return nil, fmt.Errorf("locales missing in context")
	}
	return &TemplateDataValues{
		MetaTags: MetaTags{
			Title:        fmt.Sprintf("%s - %s | %s", locales.RegisterTitle, cfg.Shorthand, locales.BrandName),
			Description:  fmt.Sprintf("%s - %s | %s", locales.RegisterTitle, cfg.Name, locales.BrandName),
			CanonicalUrl: fmt.Sprintf("/%s/%s", clubId, constants.RegisterRoute),
			ImageUrl:     "/static/ClubIcon.webp",
		},
		Club:    cfg,
		Locales: locales,
		Supplementary: map[string]interface{}{
			"EmailTaken": emailTaken,
		},
	}, nil
}

func (p RegisterPage) FuncMap() *template.FuncMap {
	return nil
}

func (p RegisterPage) AdditionalTemplates() []string {
	return []string{}
}
