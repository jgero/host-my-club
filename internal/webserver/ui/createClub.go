package ui

import (
	"context"
	"fmt"
	"html/template"
	"regexp"

	"gitlab.com/jgero/host-my-club/internal/constants"
	"gitlab.com/jgero/host-my-club/internal/content"
)

type CreateClubPage struct {
}

func (p CreateClubPage) Name() string {
	return "create"
}

func (p CreateClubPage) LayoutTemplateName() string {
	return "layout_system.html"
}

func (p CreateClubPage) PathMatcher() *regexp.Regexp {
	return regexp.MustCompile(fmt.Sprintf("^/%s$", constants.CreateClubRoute))
}

func (p CreateClubPage) CheckAccess(ctx context.Context) error {
	return nil
}

func (p CreateClubPage) TemplateData(ctx context.Context) (*TemplateDataValues, error) {
	cfg, ok := ctx.Value(constants.ClubConfigKey).(content.ClubConfig)
	if !ok {
		return nil, fmt.Errorf("club config missing in context")
	}
	locales, ok := ctx.Value(constants.SystemLocalesKey).(constants.Locales)
	if !ok {
		return nil, fmt.Errorf("locales missing in context")
	}
	return &TemplateDataValues{
		MetaTags: MetaTags{
			Title:        fmt.Sprintf("%s | %s", locales.WelcomeLabel, locales.BrandName),
			Description:  fmt.Sprintf("%s | %s", locales.WelcomeLabel, locales.BrandName),
			CanonicalUrl: fmt.Sprintf("/%s", constants.WelcomeRoute),
			ImageUrl:     "/static/ClubIcon.webp",
		},
		Club:    cfg,
		Locales: locales,
	}, nil
}

func (p CreateClubPage) FuncMap() *template.FuncMap {
	return nil
}

func (p CreateClubPage) AdditionalTemplates() []string {
	return []string{}
}
