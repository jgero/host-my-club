package ui_test

import (
	"bytes"
	"context"
	"io/fs"
	"os"
	"testing"

	"gitlab.com/jgero/host-my-club/internal/constants"
	"gitlab.com/jgero/host-my-club/internal/content"
	"gitlab.com/jgero/host-my-club/internal/webserver/ui"
)

type useWebDirFs struct {
	pathPrefix string
}

func (fs useWebDirFs) Open(name string) (fs.File, error) {
	relativeFilename := fs.pathPrefix + name
	file, err := os.Open(relativeFilename)
	if err != nil {
		return nil, err
	}
	return file, nil
}

type mockTemplateUserDto struct {
}

func (m mockTemplateUserDto) GetUserById(ctx context.Context, id string) (*content.UserById, error) {
	return &content.UserById{
		Username: "testuser",
		Userid:   "shouldnotmatter",
		Email:    "asdf@test.de",
		Groups: []string{
			"editor",
			"foo",
		},
		IsAdmin:  false,
		IsEditor: true,
	}, nil
}
func (m mockTemplateUserDto) GetAllUsers(ctx context.Context) ([]content.UserById, error) {
	return []content.UserById{
		{
			Username: "testuser",
			Userid:   "shouldnotmatter",
			Email:    "asdf@test.de",
			Groups: []string{
				"editor",
				"foo",
			},
			IsAdmin:  false,
			IsEditor: true,
		},
	}, nil
}

func TestRenderingPages(t *testing.T) {
	testRoutes := []context.Context{
		getRequestContextNoUser("/login"),
		getRequestContextNoUser("/register"),
		getRequestContextNoUser("/"),
		getRequestContextNoUser("/easily"),
		getRequestContextNoUser("/create"),
		getRequestContextNoUser("/error/500"),
		getRequestContextUserLoggedIn("/user/4993bc78-3fd0-11ec-abbf-9ed65005a454"),
	}
	tempaltes := useWebDirFs{pathPrefix: "/app/web/template/"}

	renderer := ui.TemplateRenderer{
		Templates: tempaltes,
		UserDto:   mockTemplateUserDto{},
	}

	renderer.Init()

	for _, requestContext := range testRoutes {
		var buf bytes.Buffer
		func() {
			defer func() {
				if p := recover(); p != nil {
					t.Errorf("should not panic with %v when rendering template for route %s", p, requestContext)
				}
			}()
			err := renderer.HandleRequest(requestContext, &buf)
			if err != nil {
				t.Errorf("expected renderer to not encounter error while handling request for route %s: %s", requestContext, err)
			}
			if buf.String() == "" {
				t.Errorf("expected template for route %s not to be empty", requestContext)
			}
		}()
	}
}

func getRequestContextUserLoggedIn(route string) context.Context {
	ctx := context.Background()
	ctx = context.WithValue(ctx, constants.RequestPathKey, route)
	ctx = context.WithValue(ctx, constants.UseridKey, "4993bc78-3fd0-11ec-abbf-9ed65005a454")
	ctx = context.WithValue(ctx, constants.IsAdminKey, false)
	ctx = context.WithValue(ctx, constants.IsEditorKey, true)
	ctx = context.WithValue(ctx, constants.ClubIdKey, "testclub")
	loc, _ := constants.GetLocalesForCC("en")
	ctx = context.WithValue(ctx, constants.SystemLocalesKey, loc)
	ctx = context.WithValue(ctx, constants.ClubConfigKey, content.ClubConfig{
		Id:          "testclub",
		Name:        "Test Club",
		Shorthand:   "test",
		CC:          "en",
		Description: "this is a short description for my test club",
		Palette: content.ColorPalette{
			Primary:     "#ffffff",
			Accent:      "#ffffff",
			Sheet:       "#ffffff",
			Background:  "#ffffff",
			Font:        "#ffffff",
			FontPrimary: "#ffffff",
			FontAccent:  "#ffffff",
		},
	})
	return ctx
}

func getRequestContextNoUser(route string) context.Context {
	ctx := context.Background()
	ctx = context.WithValue(ctx, constants.RequestPathKey, route)
	ctx = context.WithValue(ctx, constants.ClubIdKey, "testclub")
	loc, _ := constants.GetLocalesForCC("en")
	ctx = context.WithValue(ctx, constants.SystemLocalesKey, loc)
	ctx = context.WithValue(ctx, constants.ClubConfigKey, content.ClubConfig{
		Id:          "testclub",
		Name:        "Test Club",
		Shorthand:   "test",
		CC:          "en",
		Description: "this is a short description for my test club",
		Palette: content.ColorPalette{
			Primary:     "#ffffff",
			Accent:      "#ffffff",
			Sheet:       "#ffffff",
			Background:  "#ffffff",
			Font:        "#ffffff",
			FontPrimary: "#ffffff",
			FontAccent:  "#ffffff",
		},
	})
	return ctx
}
