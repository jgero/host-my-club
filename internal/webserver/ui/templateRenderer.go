package ui

import (
	"bytes"
	"context"
	"fmt"
	"html/template"
	"io"
	"io/fs"
	"regexp"

	"gitlab.com/jgero/host-my-club/internal/constants"
	"gitlab.com/jgero/host-my-club/internal/content"
)

type TemplateUserDto interface {
	GetUserById(context.Context, string) (*content.UserById, error)
	GetAllUsers(context.Context) ([]content.UserById, error)
}

type TemplateRenderer struct {
	Templates fs.FS
	UserDto   TemplateUserDto
	routes    []RouteHandler
}

type MetaTags struct {
	Title        string
	Description  string
	CanonicalUrl string
	ImageUrl     string
}

type TemplateDataValues struct {
	MetaTags
	Club          content.ClubConfig
	Locales       constants.Locales
	Supplementary map[string]interface{}
}

type RouteHandler interface {
	// used to match the template subdir to get the index and style templates
	Name() string
	// get the name of the layout file that is used to render the page into
	LayoutTemplateName() string
	// ran against the request path to determine which route to render
	PathMatcher() *regexp.Regexp
	// check if access is allowed
	CheckAccess(ctx context.Context) error
	// build template data by running db requests and return as map or struct
	TemplateData(ctx context.Context) (*TemplateDataValues, error)
	// OPTIONAL: function map to add when rendering the template
	FuncMap() *template.FuncMap
	// OPTIONAL: additional templates to include when rendering
	// TODO: remember to try and pass the rendered markdown as template instead of as parameter
	AdditionalTemplates() []string
}

type WrongMethodError struct {
	ProvidedMethod string
	ExpectedMethod string
}

func (e *WrongMethodError) Error() string {
	return fmt.Sprintf("HTTP method '%s' was expected but recieved '%s'", e.ExpectedMethod, e.ProvidedMethod)
}

type NoTemplateRouteMatched struct {
	UnmatchedPath string
}

func (e *NoTemplateRouteMatched) Error() string {
	return fmt.Sprintf("no route in the template renderer matched the path '%s'", e.UnmatchedPath)
}

type UnauthorizedError struct {
	Reason             string
	DeniedResourceName string
}

func (e *UnauthorizedError) Error() string {
	return fmt.Sprintf("request it not authorized to access %s because %s", e.DeniedResourceName, e.Reason)
}

func (tr *TemplateRenderer) Init() {
	tr.routes = []RouteHandler{
		LoginPage{},
		StartPage{},
		RegisterPage{},
		UserPage{
			userDto: tr.UserDto,
		},
		ErrorPage{},
		WelcomePage{},
		CreateClubPage{},
	}
}

func (tr TemplateRenderer) HandleRequest(ctx context.Context, w io.Writer) error {
	path, ok := ctx.Value(constants.RequestPathKey).(string)
	if !ok {
		return fmt.Errorf("request path is missing in context")
	}
	for _, route := range tr.routes {
		if !route.PathMatcher().MatchString(path) {
			continue
		}
		if err := route.CheckAccess(ctx); err != nil {
			return err
		}
		// it is necessary to name the template in New() like one of the files that are included
		// later in ParseFS()
		t := template.New(route.LayoutTemplateName())
		if route.FuncMap() != nil {
			t = t.Funcs(*route.FuncMap())
		}

		t = template.Must(t.ParseFS(
			tr.Templates,
			append([]string{
				route.LayoutTemplateName(),
				fmt.Sprintf("%s/index.html", route.Name()),
				fmt.Sprintf("%s/style.html", route.Name()),
			},
				route.AdditionalTemplates()...,
			)...,
		))

		templData, err := route.TemplateData(ctx)
		if err != nil {
			return fmt.Errorf("could not render template %s: %s", route.Name(), err)
		}

		buf := &bytes.Buffer{}
		if err := t.Execute(buf, *templData); err != nil {
			return fmt.Errorf("could not execute template %s: %s", route.Name(), err)
		}
		_, err = buf.WriteTo(w)
		if err != nil {
			return err
		}
		return nil
	}
	return &NoTemplateRouteMatched{UnmatchedPath: path}
}
