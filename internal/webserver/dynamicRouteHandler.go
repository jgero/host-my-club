package webserver

import (
	"embed"
	"fmt"
	"io/fs"
	"net/http"

	"gitlab.com/jgero/host-my-club/internal/constants"
	"gitlab.com/jgero/host-my-club/internal/webserver/endpoint"
	"gitlab.com/jgero/host-my-club/internal/webserver/ui"
)

//go:generate rm -rf template_copy
//go:generate cp -ru ../../web/template ./template_copy
//go:embed template_copy/*.html template_copy/**/*.html
var htmlTemplatesCopy embed.FS

func (serv *Webserver) DynamicRouteHandler() http.HandlerFunc {

	htmlTemplates, _ := fs.Sub(htmlTemplatesCopy, "template_copy")
	renderer := ui.TemplateRenderer{
		Templates: htmlTemplates,
		UserDto:   serv.UserDto,
	}
	renderer.Init()
	endpoints := endpoint.EndpointCollector{
		UserDto:   serv.UserDto,
		ConfigDto: serv.ConfigDto,
	}
	endpoints.Init()

	return func(rw http.ResponseWriter, r *http.Request) {
		path, ok := r.Context().Value(constants.RequestPathKey).(string)
		if !ok {
			serv.logger.Printf("request path is missing in context")
		}

		switch r.Method {
		case http.MethodGet:
			// all get methods are handled by the template renderer
			err := renderer.HandleRequest(r.Context(), rw)
			if err == nil {
				// body is written into response by the handler
			} else if _, ok := err.(*ui.UnauthorizedError); ok {
				serv.logger.Printf("WARNING: %s", err)
				http.Redirect(rw, r, fmt.Sprintf("/%s/%d", constants.ErrorRoute, http.StatusUnauthorized), http.StatusSeeOther)
			} else if _, ok := err.(*ui.NoTemplateRouteMatched); ok {
				serv.logger.Printf("WARNING: %s", err)
				http.Redirect(rw, r, fmt.Sprintf("/%s/%d", constants.ErrorRoute, http.StatusNotFound), http.StatusSeeOther)
			} else {
				serv.logger.Println(err)
				http.Redirect(rw, r, fmt.Sprintf("/%s/%d", constants.ErrorRoute, http.StatusInternalServerError), http.StatusSeeOther)
			}
		case http.MethodPost:
			// all post methods are handled by the endpoint collector
			redirect, action, err := endpoints.HandleRequest(r.Context(), rw)

			// check if logout is necessary
			if action == endpoint.LogoutAlways ||
				action == endpoint.LogoutOnError && err != nil ||
				action == endpoint.LogoutOnSuccess && err == nil {
				var clubid string
				if clubid, ok = r.Context().Value(constants.ClubIdKey).(string); !ok {
					serv.logger.Println("DynamicRouteHandler: clubid missing in context")
				}
				http.SetCookie(rw, &http.Cookie{
					Name:     "token",
					Value:    "",
					Path:     fmt.Sprintf("/%s", clubid),
					HttpOnly: true,
					MaxAge:   -1,
				})
			}

			if err == nil {
				http.Redirect(rw, r, redirect, http.StatusSeeOther)
			} else if _, ok := err.(*endpoint.UnauthorizedError); ok {
				serv.logger.Printf("WARNING: %s", err)
				http.Redirect(rw, r, fmt.Sprintf("/%s/%d", constants.ErrorRoute, http.StatusUnauthorized), http.StatusSeeOther)
			} else if cerr, ok := err.(*endpoint.ConflictError); ok {
				serv.logger.Printf("WARNING: %s", cerr)
				http.Redirect(rw, r, cerr.Redirect, http.StatusSeeOther)
			} else if _, ok := err.(*endpoint.BadRequestError); ok {
				serv.logger.Printf("WARNING: %s", err)
				http.Redirect(rw, r, fmt.Sprintf("%s?badRequest=true", path), http.StatusSeeOther)
			} else if _, ok := err.(*endpoint.NoEndpointRouteMatched); ok {
				serv.logger.Printf("WARNING: %s", err)
				http.Redirect(rw, r, fmt.Sprintf("/%s/%d", constants.ErrorRoute, http.StatusNotFound), http.StatusSeeOther)
			} else {
				serv.logger.Println(err)
				http.Redirect(rw, r, fmt.Sprintf("/%s/%d", constants.ErrorRoute, http.StatusInternalServerError), http.StatusSeeOther)
			}
		default:
			rw.WriteHeader(http.StatusMethodNotAllowed)
		}
	}
}
