fmt_check:
	podman build \
		-f "build/ci/formatting.Containerfile" \
		-t "host-my-club-formatting:dev" \
		$(PWD)
	podman run \
		--rm \
		host-my-club-formatting:dev

fmt_apply:
	podman build \
		-f "build/ci/formatting.Containerfile" \
		-t "host-my-club-formatting:dev" \
		$(PWD)
	podman run \
		--rm \
		-v "$(PWD):/app/code:z" \
		-e MODE=apply \
		host-my-club-formatting:dev

test:
	podman build \
		-f "build/ci/unittests.Containerfile" \
		-t "host-my-club-unittests:dev" \
		$(PWD)
	podman run \
		--rm \
		host-my-club-unittests:dev
sec:
	podman build \
		-f "build/ci/gosec.Containerfile" \
		-t "host-my-club-gosec:dev" \
		$(PWD)
	podman run \
		--rm \
		host-my-club-gosec:dev

.PHONY: fmt_check fmt_apply test sec
