package main

import (
	"github.com/gocql/gocql"
	"gitlab.com/jgero/host-my-club/internal/content"
	"gitlab.com/jgero/host-my-club/internal/webserver"
)

func main() {
	// layoutGen := &locales.StaticLayoutLoader{}
	// conn, err := store.GetObjectstoreConnection()
	// if err != nil {
	// 	fmt.Println(err.Error())
	// 	os.Exit(1)
	// }

	// server := webserver.UiWebserver{
	// 	Port:                   ":" + os.Getenv("SERVER_PORT"),
	// 	ImageResourceStoreConn: conn,
	// 	LandingpageGen:         &locales.StaticLandingpageLoader{LayoutGen: layoutGen},
	// 	ErrorpageGen:           &locales.StaticErrorpageLoader{LayoutGen: layoutGen},
	// 	BlogpageGen:            &locales.StorageBlogpageLoader{LayoutGen: layoutGen, Conn: conn},
	// 	OverviewpageGen:        &locales.StorageOverviewLoader{LayoutGen: layoutGen, Conn: conn},
	// }
	cluster := gocql.NewCluster("127.0.0.1")
	cluster.Keyspace = "hostmyclub"
	cluster.Consistency = gocql.Quorum

	server := webserver.Webserver{
		Port: ":8080",
		UserDto: content.UserCassandraDto{
			Cluster: cluster,
		},
		ConfigDto: content.ConfigCassandraDto{
			Cluster: cluster,
		},
	}
	server.Init()
	server.Start()
}
