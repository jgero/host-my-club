module gitlab.com/jgero/host-my-club

go 1.16

require (
	github.com/gocql/gocql v0.0.0-20211015133455-b225f9b53fa1
	github.com/golang-jwt/jwt/v4 v4.1.0
	github.com/google/uuid v1.3.0
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519
)
