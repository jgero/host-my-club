# Cassandra Data Model

Cassandra requires a query-first data model. Because of Cassandra's architecture with partition and
clustering keys it is very important to know how the data will be queried before modeling the data
structues.

## Queries resulting from the Application Workflow

The workflow of the application can be divided into a few major sections. The following paragraphs
dive into each of these sections and show which queries these require.

### User Profile Interactions

![](docs/data_model/user.png)

For loading the profile of a user there is not only the need of verifying the password on login, but
also fetching more detailed data about the user. This includes data about the user itself, but also
events that it is invited to.

For admins there also is an admin page where they can assign groups to users. For this all the users
for that club have to be loaded.

![](docs/data_model/user_tables.png)

To login via email and password and then load the user by its `userid` two tables are needed.
Because reads on both of these tables will only select one specific row by email or userid it is
okay to use single row partitions.

Events for a specific user are stored in a very denormalized form to enable easy queries. When a
event is created with for example 10 participants, it is stored 10 times in the
`events_by_participant` table. The rows are partitioned by the participant they are assigned to and
clustered by date and eventid, this means loading the "my-events" page for a user only requires a
read on a single partition.

### Filtered Content

![](docs/data_model/filtered_content.png)

Visiting the startpage or searchpage require filtering by visits, recency and tags. Additionally
all content has a detail view page, in which specific items are loaded by id. To provide the user
suggestions when typing the search tags a complete list of the tags also has to be loaded.

![](docs/data_model/filtered_content_tables.png)

For filtering by different dates an additional column `yyyymm`, which contains the year and month of
the date as string, is constructed when inserting and used as partition key. Clustering is then
performed on the dates and the ids. This also allows loading content by month, which comes in very
handy for loading events into a calendar.

Tags are stored in a sparate tag table with the name of the tag as key and a single `visits`
counter. This makes incrementing and decrementing usages of tags very easy when updating content. It
also helps sorting tags by amount of usages to provide more relevant suggestions. On that note, Q7
from the application workflow does not need a specific table or query. Related tags can just be
computed client-side by removing all search tags from all tags that are included in the currently
displayed content.

![](docs/data_model/filtered_content_tables_2.png)

To filter by tag and id the already established strategy of partitioning content by a month string
is used again. For the actual clustering a tag and/or id is needed.

Important to note is, that when the user is searching for multiple tags a query has to be run for
every tag the user specified. Then some client-side computation is needed to find out which element
is contained in all the queries to eliminate duplicate results and sort them by best matches.

